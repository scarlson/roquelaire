package main

import (
	"database/sql"
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

var CurCache = make(map[string]*Currency)

func CacheCurs() {
	curs, _ := db.Query("select code, description, separator, valveid, rate, sign, prefix, suffix from currency where valve is true;")
	var code sql.NullString
	var description sql.NullString
	var separator sql.NullString
	var valveid sql.NullString
	var rate sql.NullString
	var sign sql.NullString
	var prefix sql.NullString
	var suffix sql.NullString
	for curs.Next() {
		curs.Scan(&code, &description, &separator, &valveid, &rate, &sign, &prefix, &suffix)
		CurCache[strings.ToLower(code.String)] = &Currency{Code: code.String, Description: description.String, Separator: separator.String, Valveid: valveid.String, Rate: rate.String, Sign: sign.String, Prefix: prefix.String, Suffix: suffix.String}
	}
}

type Currency struct {
	Code        string
	Description string
	Separator   string
	Valveid     string
	Rate        string
	Sign        string
	Prefix      string
	Suffix      string
}

type Price struct {
	Name  string `json:"name"`
	Price string `json:"price"`
}

func ValidPrice(price string) bool {
	i, e := strconv.ParseFloat(string(price), 64)
	if e != nil {
		LogError(e, "ValidPrice()", "Low")
		return false
	}
	if i < 0.0 || i > 400.00 {
		//fmt.Println(price, "not a valid price!")
		return false
	}
	if strings.Count(price, ".") == 1 &&
		//strings.Count(price, "$") == 1 &&
		strings.Count(price, ".")+
			//strings.Count(price, "$")+
			strings.Count(price, "0")+
			strings.Count(price, "1")+
			strings.Count(price, "2")+
			strings.Count(price, "3")+
			strings.Count(price, "4")+
			strings.Count(price, "5")+
			strings.Count(price, "6")+
			strings.Count(price, "7")+
			strings.Count(price, "8")+
			strings.Count(price, "9") == len(price) {
		return true
	}
	fmt.Println(price, "not a valid price!")
	return false
}

func SinglePrice(price string) []string {
	var p []string
	for _, c := range CurCache {
		p = append(p, "<span class='"+c.Code+"'>"+c.Prefix+ConvertPrice(price, c.Code)+c.Suffix+"</span>")
	}
	sort.Strings(p)
	return p
}

func DoublePrice(low string, high string) []string {
	var p []string
	for _, c := range CurCache {
		p = append(p, "<span class='"+c.Code+"'>"+c.Prefix+ConvertPrice(low, c.Code)+c.Suffix+" — "+c.Prefix+ConvertPrice(high, c.Code)+c.Suffix+"</span>")
	}
	sort.Strings(p)
	return p
}

func ReversePrice(price, curid string) string {
	fmt.Println("Reverse received:", price)
	re := regexp.MustCompile("[^0-9.,]")
	if cur, ok := CurCache[strings.ToLower(curid)]; ok {
		price = re.ReplaceAllString(price, "")
		price = strings.Replace(price, cur.Separator, ".", -1)
		p, err := strconv.ParseFloat(price, 64)
		if err != nil {
			fmt.Println("Reverse err1:", err)
		}
		y, err := strconv.ParseFloat(cur.Rate, 64)
		if err != nil {
			fmt.Println("Reverse err2:", err)
		}
		l := fmt.Sprintf("%.2f", (p / y))
		fmt.Println("Reverse sent:", l)
		return l
	}
	fmt.Println("ReversePrice: bad curid:", curid)
	return price
}

func ConvertPrice(price, curid string) string {
	if cur, ok := CurCache[strings.ToLower(curid)]; ok {
		p, _ := strconv.ParseFloat(price, 64)
		y, _ := strconv.ParseFloat(cur.Rate, 64)
		l := fmt.Sprintf("%.2f", (p * y))
		l = strings.Replace(l, ".", cur.Separator, -1)
		return l
	}
	return price
}
