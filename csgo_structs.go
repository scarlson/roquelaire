package main

import (
	"fmt"
	"github.com/gedex/inflector"
	"strconv"
	"strings"
	"time"
)

var qualities = map[string]int{
	"quality":          0,
	"base grade":       1,
	"consumer grade":   2,
	"mil-spec grade":   3,
	"industrial grade": 4,
	"restricted":       5,
	"classified":       6,
	"high grade":       7,
	"covert":           8,
	"remarkable":       9,
	"exotic":           10,
	"contraband":       11,
}

type Pricer interface {
	Price() []string
	Image() string
}

type Base struct {
	Name  string
	Type  string
	image string
}

func (self *Base) Null() {
	self.Name = ""
	self.Type = ""
	self.image = ""
}

func (self *Base) Plural() string {
	return inflector.Pluralize(self.Name)
}

func (self *Base) Image() string {
	if self.image != "" {
		return self.image
	}
	for _, guns := range GunCache {
		for _, gun := range guns {
			if gun.Base == self.Name {
				self.image = gun.image
				return self.image
			}
		}
	}
	return ""
}

type Type struct {
	Name  string
	image string
}

func (self *Type) Null() {
	self.Name = ""
	self.image = ""
}

func (self *Type) Plural() string {
	return inflector.Pluralize(self.Name)
}

func (self *Type) Image() string {
	if self.image != "" {
		return self.image
	}
	for _, guns := range GunCache {
		for _, gun := range guns {
			if gun.Type == self.Name {
				self.image = gun.image
				return self.image
			}
		}
	}
	return ""
}

type CSContainer struct {
	Name       string
	Id         string
	keystring  string
	Contents   string
	Guns       []*GunBucket
	Collection string
	item       *CSItem
	key        *CSKey
}

func (self *CSContainer) Null() {
	self.Name = ""
	self.Id = ""
	self.keystring = ""
	self.Contents = ""
	self.Guns = nil
	self.Collection = ""
	self.item = nil
	self.key = nil
}

func (self *CSContainer) HasCollection() bool {
	return self.Collection != ""
}

func (self *CSContainer) Contains(gun *CSItem) bool {
	for _, bucket := range self.Guns {
		for _, g := range bucket.Guns {
			if g.Name == gun.Name {
				return true
			}
		}
	}
	return false
}

func (self *CSContainer) Item() *CSItem {
	if self.item != nil {
		return self.item
	}
	for _, guns := range GunCache {
		for _, gun := range guns {
			if self.Name == gun.Name {
				self.item = gun
				return self.item
			}
		}
	}
	return nil
}

func (self *CSContainer) HasKey() bool {
	if self.Key() != nil {
		return true
	}
	return false
}

func (self *CSContainer) Key() *CSKey {
	if self.key != nil {
		return self.key
	}
	if _, ok := KeyCache[self.keystring]; ok {
		self.key = KeyCache[self.keystring]
		return self.key
	}
	return nil
}

type CSKey struct {
	Name             string
	Id               string
	containersstring []string
	item             *CSItem
	containers       []*CSContainer
}

func (self *CSKey) Null() {
	self.Name = ""
	self.Id = ""
	self.containersstring = nil
	self.item = nil
	self.containers = nil
}

func (self *CSKey) OnlyOne() bool {
	return self.ContainerCount() <= 1
}

func (self *CSKey) ContainerCount() int {
	return len(self.Containers())
}

func (self *CSKey) Containers() []*CSContainer {
	if len(self.containers) > 0 {
		return self.containers
	}
	containers := []*CSContainer{}
	for _, box := range BoxCache {
		if box.keystring == self.Id {
			containers = append(containers, box)
		}
	}
	if len(containers) > 0 {
		self.containers = containers
		return self.containers
	}
	return nil
}

func (self *CSKey) Item() *CSItem {
	if self.item != nil {
		return self.item
	}
	for _, guns := range GunCache {
		for _, gun := range guns {
			if self.Name == gun.Name {
				self.item = gun
				return self.item
			}
		}
	}
	return nil
}

type CSItem struct {
	Base        string
	Exterior    string
	Quality     string
	Type        string
	Name        string
	price       string
	Description string
	Link        string
	Stattrak    bool
	Souvenir    bool
	Skin        string
	Star        bool
	Added       time.Time
	Collection  string
	image       string
	InGame      string
	container   *CSContainer
}

func (self *CSItem) Null() {
	self.Base = ""
	self.Exterior = ""
	self.Quality = ""
	self.Type = ""
	self.Name = ""
	self.price = ""
	self.Description = ""
	self.Link = ""
	self.Stattrak = false
	self.Souvenir = false
	self.Skin = ""
	self.Star = false
	self.Collection = ""
	self.image = ""
	self.InGame = ""
	self.container = nil
}

func (self *CSItem) Image() string {
	return self.image
}

func (self *CSItem) ConvertPrice(curid string) string {
	if cur, ok := CurCache[strings.ToLower(curid)]; ok {
		p, _ := strconv.ParseFloat(self.price, 64)
		y, _ := strconv.ParseFloat(cur.Rate, 64)
		if curid != "" {
			price := fmt.Sprintf("%s%.2f%s", cur.Prefix, (p * y), cur.Suffix)
			return strings.Replace(price, ".", cur.Separator, -1)
		}
		return fmt.Sprintf("$%.2f", (p * y))
	}
	return self.price
}

func (self *CSItem) Price() []string {
	return SinglePrice(self.price)
}

func (self *CSItem) Container() *CSContainer {
	if self.container != nil {
		return self.container
	}
	for _, box := range BoxCache {
		for _, gun := range box.Guns {
			if gun.Base == self.Base && gun.Skin == self.Skin {
				self.container = box
				return self.container
			}
		}
	}
	return nil
}

func (self *CSItem) NullGame() bool {
	if self.InGame == "" {
		return true
	}
	return false
}

func (self *CSItem) HasCollection() bool {
	if self.Collection != "" {
		return true
	}
	return false
}

func (self *CSItem) HasSkin() bool {
	if self.Skin != "" {
		return true
	}
	return false
}

type GunBucketContainer struct {
	Name    string
	Buckets []*GunBucket
}

func (self *GunBucketContainer) Null() {
	self.Name = ""
	self.Buckets = nil
}

func (self *GunBucketContainer) Plural() string {
	return inflector.Pluralize(self.Name)
}

func (self *GunBucketContainer) OnlyOne() bool {
	return self.BucketSize() == 1
}

func (self *GunBucketContainer) BucketSize() int {
	return len(self.Buckets)
}

func (self *GunBucketContainer) RandomFour() []*GunBucket {
	if len(self.Buckets) > 4 {
		return self.Buckets[0:4]
	}
	return self.Buckets
}

type GunBucket struct {
	Base                  string
	Quality               string
	Type                  string
	Low                   string
	High                  string
	Description           string
	Stattrak              bool
	Souvenir              bool
	Skin                  string
	Star                  bool
	Collection            string
	Guns                  []*CSItem
	containers            []*CSContainer
	key                   *CSKey
	image                 string
	plain                 Pricer
	holo                  Pricer
	foil                  Pricer
	battlescarred         Pricer
	wellworn              Pricer
	fieldtested           Pricer
	minimalwear           Pricer
	factorynew            Pricer
	plainstattrak         Pricer
	holostattrak          Pricer
	foilstattrak          Pricer
	battlescarredstattrak Pricer
	wellwornstattrak      Pricer
	fieldtestedstattrak   Pricer
	minimalwearstattrak   Pricer
	factorynewstattrak    Pricer
	plainsouvenir         Pricer
	holosouvenir          Pricer
	foilsouvenir          Pricer
	battlescarredsouvenir Pricer
	wellwornsouvenir      Pricer
	fieldtestedsouvenir   Pricer
	minimalwearsouvenir   Pricer
	factorynewsouvenir    Pricer
}

func (self *GunBucket) Null() {
	self.Base = ""
	self.Quality = ""
	self.Type = ""
	self.Low = ""
	self.High = ""
	self.Description = ""
	self.Stattrak = false
	self.Souvenir = false
	self.Skin = ""
	self.Star = false
	self.Collection = ""
	self.Guns = nil
	self.containers = nil
	self.key = nil
	self.image = ""
	self.plain = nil
	self.holo = nil
	self.foil = nil
	self.battlescarred = nil
	self.wellworn = nil
	self.fieldtested = nil
	self.minimalwear = nil
	self.factorynew = nil
	self.plainstattrak = nil
	self.holostattrak = nil
	self.foilstattrak = nil
	self.battlescarredstattrak = nil
	self.wellwornstattrak = nil
	self.fieldtestedstattrak = nil
	self.minimalwearstattrak = nil
	self.factorynewstattrak = nil
	self.plainsouvenir = nil
	self.holosouvenir = nil
	self.foilsouvenir = nil
	self.battlescarredsouvenir = nil
	self.wellwornsouvenir = nil
	self.fieldtestedsouvenir = nil
	self.minimalwearsouvenir = nil
	self.factorynewsouvenir = nil
}

func (self *GunBucket) ConvertPrice(curid string) (string, string) {
	if cur, ok := CurCache[strings.ToLower(curid)]; ok {
		l, _ := strconv.ParseFloat(self.Low, 64)
		h, _ := strconv.ParseFloat(self.High, 64)
		y, _ := strconv.ParseFloat(cur.Rate, 64)
		if curid != "" {
			low := fmt.Sprintf("%s%.2f%s", cur.Prefix, (l * y), cur.Suffix)
			low = strings.Replace(low, ".", cur.Separator, -1)
			high := fmt.Sprintf("%s%.2f%s", cur.Prefix, (h * y), cur.Suffix)
			high = strings.Replace(high, ".", cur.Separator, -1)
			return low, high
		}
		low := fmt.Sprintf("$%.2f", l)
		high := fmt.Sprintf("$%.2f", h)
		return low, high
	}
	return "", ""
}

func (self *GunBucket) Price() []string {
	if self.Low == self.High {
		return SinglePrice(self.Low)
	}
	return DoublePrice(self.Low, self.High)
}

func (self *GunBucket) Plural() string {
	return inflector.Pluralize(self.Base)
}

func (self *GunBucket) Size() int {
	return len(self.Guns)
}

func (self *GunBucket) OnlyOne() bool {
	return self.Size() <= 1
}

func (self *GunBucket) Containers() []*CSContainer {
	if len(self.containers) > 0 {
		return self.containers
	}
	if self.Type == "Key" {
		if key, ok := KeyCache[strings.ToLower(self.Base)]; ok {
			self.containers = key.Containers()
			return self.containers
		}
	}
	if self.Type == "Container" {
		if _, ok := BoxCache[strings.ToLower(self.Base)]; ok {
			self.containers = []*CSContainer{BoxCache[strings.ToLower(self.Base)]}
			return self.containers
		}
	}
	for _, box := range BoxCache {
		for _, bucket := range box.Guns {
			if self.Base == bucket.Base && self.Skin == bucket.Skin {
				self.containers = []*CSContainer{box}
				return self.containers
			}
		}
	}
	return nil
}

func (self *GunBucket) Key() *CSKey {
	if self.key != nil {
		return self.key
	}
	for _, container := range self.Containers() {
		if container.Key() != nil {
			self.key = container.Key()
			return self.key
		}
	}
	return nil
}

func (self *GunBucket) HasCollection() bool {
	if self.Collection != "" {
		return true
	}
	return false
}

func (self *GunBucket) HasSkin() bool {
	if self.Skin != "" {
		return true
	}
	return false
}

func (self *GunBucket) BestImage() string {
	if self.image != "" {
		return self.image
	}
	if self.FactoryNew() != nil {
		if self.FactoryNew().Image() != "" {
			self.image = self.FactoryNew().Image()
			return self.image
		}
	}
	if self.MinimalWear() != nil {
		if self.MinimalWear().Image() != "" {
			self.image = self.MinimalWear().Image()
			return self.image
		}
	}
	if self.FieldTested() != nil {
		if self.FieldTested().Image() != "" {
			self.image = self.FieldTested().Image()
			return self.image
		}
	}
	if self.WellWorn() != nil {
		if self.WellWorn().Image() != "" {
			self.image = self.WellWorn().Image()
			return self.image
		}
	}
	if self.BattleScarred() != nil {
		if self.BattleScarred().Image() != "" {
			self.image = self.BattleScarred().Image()
			return self.image
		}
	}
	if len(self.Guns) > 0 {
		self.image = self.Guns[0].Image()
		return self.image
	}
	return ""
}

func (self *GunBucket) Plain() Pricer {
	if self.plain != nil {
		return self.plain
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "" && gun.Stattrak == false {
			self.plain = gun
			return self.plain
		}
	}
	return nil
}

func (self *GunBucket) Holo() Pricer {
	if self.holo != nil {
		return self.holo
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Holo" && gun.Type == "Sticker" {
			self.holo = gun
			return self.holo
		}
	}
	return nil
}

func (self *GunBucket) Foil() Pricer {
	if self.foil != nil {
		return self.foil
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Foil" && gun.Type == "Sticker" {
			self.foil = gun
			return self.foil
		}
	}
	return nil
}

func (self *GunBucket) BattleScarred() Pricer {
	if self.battlescarred != nil {
		return self.battlescarred
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Battle-Scarred" && gun.Stattrak == false && gun.Souvenir == false {
			self.battlescarred = gun
			return self.battlescarred
		}
	}
	return nil
}

func (self *GunBucket) WellWorn() Pricer {
	if self.wellworn != nil {
		return self.wellworn
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Well-Worn" && gun.Stattrak == false && gun.Souvenir == false {
			self.wellworn = gun
			return self.wellworn
		}
	}
	return nil
}

func (self *GunBucket) FieldTested() Pricer {
	if self.fieldtested != nil {
		return self.fieldtested
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Field-Tested" && gun.Stattrak == false && gun.Souvenir == false {
			self.fieldtested = gun
			return self.fieldtested
		}
	}
	return nil
}

func (self *GunBucket) MinimalWear() Pricer {
	if self.minimalwear != nil {
		return self.minimalwear
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Minimal Wear" && gun.Stattrak == false && gun.Souvenir == false {
			self.minimalwear = gun
			return self.minimalwear
		}
	}
	return nil
}

func (self *GunBucket) FactoryNew() Pricer {
	if self.factorynew != nil {
		return self.factorynew
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Factory New" && gun.Stattrak == false && gun.Souvenir == false {
			self.factorynew = gun
			return self.factorynew
		}
	}
	return nil
}

func (self *GunBucket) PlainStatTrak() Pricer {
	if self.plainstattrak != nil {
		return self.plainstattrak
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "" && gun.Stattrak == true {
			self.plainstattrak = gun
			return self.plainstattrak
		}
	}
	return nil
}

func (self *GunBucket) BattleScarredStatTrak() Pricer {
	if self.battlescarredstattrak != nil {
		return self.battlescarredstattrak
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Battle-Scarred" && gun.Stattrak == true {
			self.battlescarredstattrak = gun
			return self.battlescarredstattrak
		}
	}
	return nil
}

func (self *GunBucket) WellWornStatTrak() Pricer {
	if self.wellwornstattrak != nil {
		return self.wellwornstattrak
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Well-Worn" && gun.Stattrak == true {
			self.wellwornstattrak = gun
			return self.wellwornstattrak
		}
	}
	return nil
}

func (self *GunBucket) FieldTestedStatTrak() Pricer {
	if self.fieldtestedstattrak != nil {
		return self.fieldtestedstattrak
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Field-Tested" && gun.Stattrak == true {
			self.fieldtestedstattrak = gun
			return self.fieldtestedstattrak
		}
	}
	return nil
}

func (self *GunBucket) MinimalWearStatTrak() Pricer {
	if self.minimalwearstattrak != nil {
		return self.minimalwearstattrak
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Minimal Wear" && gun.Stattrak == true {
			self.minimalwearstattrak = gun
			return self.minimalwearstattrak
		}
	}
	return nil
}

func (self *GunBucket) FactoryNewStatTrak() Pricer {
	if self.factorynewstattrak != nil {
		return self.factorynewstattrak
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Factory New" && gun.Stattrak == true {
			self.factorynewstattrak = gun
			return self.factorynewstattrak
		}
	}
	return nil
}

func (self *GunBucket) PlainSouvenir() Pricer {
	if self.plainsouvenir != nil {
		return self.plainsouvenir
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "" && gun.Souvenir == true {
			self.plainsouvenir = gun
			return self.plainsouvenir
		}
	}
	return nil
}

func (self *GunBucket) BattleScarredSouvenir() Pricer {
	if self.battlescarredsouvenir != nil {
		return self.battlescarredsouvenir
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Battle-Scarred" && gun.Souvenir == true {
			self.battlescarredsouvenir = gun
			return self.battlescarredsouvenir
		}
	}
	return nil
}

func (self *GunBucket) WellWornSouvenir() Pricer {
	if self.wellwornsouvenir != nil {
		return self.wellwornsouvenir
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Well-Worn" && gun.Souvenir == true {
			self.wellwornsouvenir = gun
			return self.wellwornsouvenir
		}
	}
	return nil
}

func (self *GunBucket) FieldTestedSouvenir() Pricer {
	if self.fieldtestedsouvenir != nil {
		return self.fieldtestedsouvenir
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Field-Tested" && gun.Souvenir == true {
			self.fieldtestedsouvenir = gun
			return self.fieldtestedsouvenir
		}
	}
	return nil
}

func (self *GunBucket) MinimalWearSouvenir() Pricer {
	if self.minimalwearsouvenir != nil {
		return self.minimalwearsouvenir
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Minimal Wear" && gun.Souvenir == true {
			self.minimalwearsouvenir = gun
			return self.minimalwearsouvenir
		}
	}
	return nil
}

func (self *GunBucket) FactoryNewSouvenir() Pricer {
	if self.factorynewsouvenir != nil {
		return self.factorynewsouvenir
	}
	for _, gun := range self.Guns {
		if gun.Exterior == "Factory New" && gun.Souvenir == true {
			self.factorynewsouvenir = gun
			return self.factorynewsouvenir
		}
	}
	return nil
}

type BaseByName []*Base

func (a BaseByName) Len() int      { return len(a) }
func (a BaseByName) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a BaseByName) Less(i, j int) bool {
	return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name)
}

type TypeByName []*Type

func (a TypeByName) Len() int      { return len(a) }
func (a TypeByName) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a TypeByName) Less(i, j int) bool {
	return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name)
}

type GBByName []*GunBucket

func (a GBByName) Len() int           { return len(a) }
func (a GBByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a GBByName) Less(i, j int) bool { return strings.ToLower(a[i].Base) < strings.ToLower(a[j].Base) }

type GBByQuality []*GunBucket

func (a GBByQuality) Len() int      { return len(a) }
func (a GBByQuality) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a GBByQuality) Less(i, j int) bool {
	return qualities[strings.ToLower(a[i].Quality)] < qualities[strings.ToLower(a[j].Quality)]
}

type GBCByName []*GunBucketContainer

func (a GBCByName) Len() int           { return len(a) }
func (a GBCByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a GBCByName) Less(i, j int) bool { return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name) }

type GBCByQuality []*GunBucketContainer

func (a GBCByQuality) Len() int      { return len(a) }
func (a GBCByQuality) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a GBCByQuality) Less(i, j int) bool {
	return qualities[strings.ToLower(a[i].Name)] < qualities[strings.ToLower(a[j].Name)]
}
