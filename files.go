package main

import (
	"io/ioutil"
	"sort"
)

func CurJS() {
	var curs []string
	for cur, _ := range CurCache {
		curs = append(curs, cur)
	}
	sort.Strings(curs)
	var js string
	js = `function setCur(cur) {
    document.cookie = "cur=" + cur + ";path=/";
    document.location = document.location;
}` + "\n"
	for _, cur := range curs {
		js = js + `function set` + CurCache[cur].Code + `(){setCur("` + CurCache[cur].Code + `");}` + "\n"
		js = js + `$("#curmenu").append('<li><a id="` + CurCache[cur].Code + `" href="">` + CurCache[cur].Code + `</a></li>');` + "\n"
		js = js + `$("#` + CurCache[cur].Code + `")[0].addEventListener("click", set` + CurCache[cur].Code + `, false);` + "\n"
	}
	err := ioutil.WriteFile("./static/js/currency.js", []byte(js), 0644)
	if err != nil {
		panic(err)
	}
}

func CurCSS() {
	var css string
	for _, cur := range CurCache {
		css = css + "." + cur.Code + " {display:none;}\n"
	}
	err := ioutil.WriteFile("./static/css/currency.css", []byte(css), 0644)
	if err != nil {
		panic(err)
	}
}
