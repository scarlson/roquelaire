#!/usr/bin/env python
import psycopg2
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def CSGOImageFix():
    query = "select distinct name, image from csitem where image like '%360fx360f';"
    cur.execute(query)
    names = cur.fetchall()
    for n in names:
        if '360f' in n[1]:
            i = CSItem(key='name', value=n[0], db=conn)
            if i._exists:
                #print "saving"
                image = n[1].replace("360fx360f", "")
                i.image = image
                i.save()


class CSItem(BaseObject):
    pass


if __name__ == "__main__":
    CSGOImageFix()
