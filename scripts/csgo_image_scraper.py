#!/usr/bin/env python
import psycopg2
import urllib2
import datetime
import time
from bs4 import BeautifulSoup
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

TRADEMARK = u"\u2122"
STAR = u"\u2605"


def extract(name, url):
    # print "Extracting", name, "at", url
    try:
        page = urllib2.urlopen(url)  # raw html
        results = page.read()
    except urllib2.HTTPError:
        time.sleep(1)
        extract(name, url)
        return
    raw = BeautifulSoup(results)
    msg = raw.select('div[class=market_listing_table_message]')
    if msg:
        print "Msg:", msg[0].string.strip()
    if len(msg) > 0 and "There are no listings for this item." in msg[0].string:
        return
    #if base == na and len(z) > 0:

    if len(raw.select('div[class=market_listing_largeimage]')) < 1:
        return
    img = raw.select('div[class=market_listing_largeimage]')[0]
    image = None
    if img:
        image = img.select('img')[0].get('src')
        image = "/".join(image.split('/')[0:-1]) + "/"  # image

    if image:
        try:
            i = CSItem(key='name', value=name, db=conn)
            if i._exists:
                #print "saving"
                i.image = image
                i.save()
            else:
                print name, "doesn't exist?"
        except:
            raise
    else:
        print "No image found for", name


def CSGOImageScan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct name, link from csitem where image is null;"
    cur.execute(query)
    names = cur.fetchall()
    print len(names), "items without image."
    count = 0
    for t in names:
        try:
            #time.sleep(1)
            extract(t[0], t[1])
            count += 1
            #print count, "items updated."
        except KeyboardInterrupt:
            return
        except Exception as err:
            raise
            e = Error(key='time', value=datetime.datetime.now(), db=conn)
            e.source = "CSGO Image Scraper"
            e.severity = "Low"
            e.error = str(err)
            e.save()


class CSItem(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    CSGOImageScan()
