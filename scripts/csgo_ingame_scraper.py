#!/usr/bin/env python
import psycopg2
import urllib2
import datetime
import time
import demjson
from bs4 import BeautifulSoup
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

TRADEMARK = u"\u2122"
STAR = u"\u2605"

types = [
    "Shotgun",
    "Knife",
    "Sniper Rifle",
    "Smg",
    "Rifle",
    "Pistol",
    "Machinegun"
]


def extract(name, url):
    # print "Extracting", name, "at", url
    try:
        page = urllib2.urlopen(url)  # raw html
        results = page.read()
    except urllib2.HTTPError:
        time.sleep(1)
        extract(name, url)
        return
    raw = BeautifulSoup(results)  # we only need to soup the unescaped json html response
    metadata = ""

    msg = raw.select('div[class=market_listing_table_message]')
    if len(msg) > 0 and msg[0].string:
        print "Msg:", msg[0].string.strip()
        return
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string and "inspect in game..." in script.string.lower():
            metadata = script.string
            ty = metadata.lower()
            ty = ty.split("var g_rgassets = ")[1].split("var g_rgcurrency = ")[0].strip()
            ty = ty.replace(";", "")
            #print ty[15805:]
            try:
                ty = demjson.decode(ty, strict=False)
            except KeyboardInterrupt:
                return
            except:
                print "|||", ty[-20:], "|||"
                print url
                return
            owner_steamid = ""
            assetid = ""
            for vsn in ty["730"]:
                for listing in ty["730"][vsn]:
                    owner_steamid = ty["730"][vsn][listing]["owner"]
                    assetid = listing
                    if "actions" in [key for key in ty["730"][vsn][listing]]:
                        if len(ty["730"][vsn][listing]["actions"]) > 0:
                            flag = False
                            if (("name" in [key for key in ty["730"][vsn][listing]["actions"][0]]) and ty["730"][vsn][listing]["actions"][0]["name"] == "inspect in game..."):
                                    flag = True
                            if flag and "link" in [key for key in ty["730"][vsn][listing]["actions"][0]]:
                                z = ty["730"][vsn][listing]["actions"][0]["link"]
                                z = z.replace("s%owner_steamid%", "S" + str(owner_steamid))
                                z = z.replace("a%assetid%d", "A" + str(assetid) + "D")
                                try:
                                    i = CSItem(key='name', value=name, db=conn)
                                    if i._exists:
                                        #print "saving"
                                        i.ingame = z
                                        i.save()
                                        break
                                    else:
                                        print name, "doesn't exist?"
                                except:
                                    raise


def CSGOInGameScan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct name, link from csitem where ingame is null and type = ANY(%s);"
    query = cur.mogrify(query, (types,))
    cur.execute(query)
    names = cur.fetchall()
    print len(names), "items without ingame."
    count = 0
    for t in names:
        try:
            #time.sleep(1)
            extract(t[0], t[1])
            count += 1
            #print count, "items updated."
        except KeyboardInterrupt:
            return
        except ValueError as err:
            print err
        except Exception as err:
            raise
            e = Error(key='time', value=datetime.datetime.now(), db=conn)
            e.source = "CSGO InGame Scraper"
            e.severity = "Low"
            e.error = str(err)
            e.save()
    print len(names), "expected", count, "items updated."


class CSItem(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    CSGOInGameScan()
