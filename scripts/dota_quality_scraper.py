#!/usr/bin/env python
import psycopg2
import datetime
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

QUALITIES = [
    "Inscribed",
    "Elder",
    "Genuine",
    "Unusual",
    "Heroic",
    "Cursed",
    "Self-Made",
    "Frozen",
    "Corrupted",
    "Favored",
    "Autographed",
    "Ascendant",
    "Auspicious",
    "Standard",
    "Community",
    "Valve",
    "Customized",
    "Completed",
    "Legacy",
    "Exalted"
]


def extract(name):
    z = ""
    n = name.split(" ")
    if n[0].lower() in [q.lower() for q in QUALITIES]:
        z = n[0]
    if z != "":
        try:
            i = Item(key='name', value=name, db=conn)
            if i._exists:
                i.quality = z
                i.save()
        except Exception as err:
            e = Error(key='time', value=datetime.datetime.now(), db=conn)
            e.source = "Quality Scraper"
            e.severity = "Low"
            e.error = str(err)
            e.save()
    else:
        z = "Standard"
        try:
            i = Item(key='name', value=name, db=conn)
            if i._exists:
                i.quality = z
                i.save()
        except:
            pass
        print "No quality found for", name


def QualityScan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct name from item where quality is null;"
    cur.execute(query)
    names = cur.fetchall()
    print len(names), "items without quality."
    count = 0
    for t in names:
        try:
            if t[0]:
                extract(t[0])
                count += 1
                print count, "items updated."
        except KeyboardInterrupt:
            break
        except:
            raise


class Item(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    QualityScan()
