#!/usr/bin/env python
import psycopg2
import urllib2
import json
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields
steam_api_key = "9685698F1B42829AD1024539E8BA196F"
schema = "http://api.steampowered.com/IEconItems_570/GetSchema/v0001/?format=json&language=en&key=" + steam_api_key
schema2 = "https://raw.githubusercontent.com/SteamDatabase/SteamTracking/master/ItemSchema/Dota2.json"

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def parseKillEaters(killeaters):
    for ke in killeaters:
        k = DotaKillEater(key='id', value=ke['type'], db=conn)
        k.name = ke['type_name']
        k.save()


def parseSets(sets, items):
    for s in sets:
        img = None
        for item in items:
            try:
                if s['store_bundle'] == item['name']:
                    img = item['image_url_large']
                    break
            except:
                if s['name'] == item['name']:
                    img = item['image_url_large']
                    break
                if s['name'] + ' Set' == item['name']:
                    img = item['image_url_large']
                    break
        if img:
            iset = DotaSet(key="id", value=s['item_set'], db=conn)
            iset.name = s['name']
            iset.items = s['items']
            iset.image = img
            iset.save()


def parseItems(items):
    for item in items:
        if item['defindex'] != 'default':
            i = DotaBaseItem(key='id', value=item['defindex'], db=conn)
            if i._exists:
                i.image = item['image_url_large']
                if not i.image:
                    i.image = item['image_url']
                i.type = item['item_type_name'].title()
                i.name = item['name']
                try:
                    i.description = item["item_description"]
                except KeyError:
                    pass
                try:
                    i.set = item["item_set"]
                except KeyError:
                    pass
                i.save()


def parseQualities(results):
    qualities = results['qualities']
    qualityNames = results['qualityNames']
    for q in qualities:
        qual = DotaQuality(key='id', value=qualities[q], db=conn)
        qual.name = qualityNames[q]
        qual.code = q
        qual.save()


def SchemaScan():
    # open game item schema
    print "loading the schema"
    try:
        page = urllib2.urlopen(schema)  # raw json
        results = json.loads(page.read())['result']
    except urllib2.HTTPError:
        page = urllib2.urlopen(schema2)  # raw json
        results = json.loads(page.read())['result']
    parseQualities(results)
    items = results['items']
    parseItems(items)
    sets = results['item_sets']
    parseSets(sets, items)
    killeaters = results['kill_eater_score_types']
    parseKillEaters(killeaters)


class Error(BaseObject):
    pass


class DotaBaseItem(BaseObject):
    pass


class DotaSet(BaseObject):
    pass


class DotaQuality(BaseObject):
    pass


class DotaKillEater(BaseObject):
    pass


if __name__ == "__main__":
    print 'starting'
    SchemaScan()
    print 'done'
