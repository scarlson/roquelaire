#!/usr/bin/env python
import psycopg2
import urllib
import urllib2
import json
import datetime
import time
import demjson
from bs4 import BeautifulSoup
from base import BaseObject
from math import trunc

url = "http://steamcommunity.com/market/search?q=appid%3A570"
url2 = "http://steamcommunity.com/market/search/render/?query=appid%3A730"
pagination = "#p\d+"
pagination2 = "&start=0&count=100"

count = 10
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


TRADEMARK = u"\u2122"
STAR = u"\u2605"

# search result fields
item = "market_listing_row_link"
price = "market_table_value"  # span child
itemname = "market_listing_item_name"
itemgame = "market_listing_game_name"


query = "select distinct name from cscollection;"
cur.execute(query)
colls = cur.fetchall()
collections = []
for c in colls:
    collections.append(c[0])

coll_types = [
    "Shotgun",
    "Sniper Rifle",
    "Smg",
    "Rifle",
    "Pistol",
    "Machinegun"
]

types = [
    "Shotgun",
    "Sniper Rifle",
    "Smg",
    "Rifle",
    "Pistol",
    "Machinegun",
    "Knife"
]

# open url
# get max page count, set maxpage count
# iterate url+pagination and grab each item
# save the item into sqlite3


def extract(soup):
    name = soup.select('span[class=' + itemname + ']')[0].string.strip()  # name
    item = CSItem(key="name", value=name, db=conn)
    item.name = name
    item.game = soup.select('span[class=' + itemgame + ']')[0].string  # game
    item.link = soup.get('href')  # link

    if item.added is None:
        item.added = datetime.datetime.now()

    p = soup.select('span[class=' + price + ']')[0].find('span').text
    if p[0] == "$":
        item.price = p[1::]
        hasSkin = False
        if "StatTrak" in item.name:
            item.stattrak = True
        if STAR in item.name:
            item.star = True
        if "Souvenir" in item.name:
            item.souvenir = True
        item.base = item.name
        if "(" in item.name:
            item.exterior = item.name.split('(')[1].split(')')[0].strip()
            if item.exterior == "H":
                item.exterior = "Holo"
        if "|" in item.name:
            hasSkin = True
            item.skin = item.base.split("|")[1].split('(')[0].strip()
        if TRADEMARK in item.base:
            item.base = item.base.split(TRADEMARK)[1]
        if "StatTrak" in item.base:
            item.base = item.base.encode('ascii', 'replace').split("StatTrak")[1]
        if STAR in item.base:
            item.base = item.base.split(STAR)[1]
        if "|" in item.base:
            item.base = item.base.split("|")[0]
        if "Souvenir" == item.base.split(" ")[0]:
            item.base = item.base.replace("Souvenir", "")
        item.base = item.base.strip()

        if hasSkin and item.skin == "":
            print item.name, "couldn't extract skin..."
            return

        needImage = False
        needQual = False
        if item.image is None or len(item.image) < 1:
            needImage = True
        if item.quality is None or len(item.quality) < 1:
            needQual = True
        if needImage or needQual:
            try:
                item.image, item.quality, item.collection, item.ingame = parsePage(item.link)
            except TypeError:
                pass
        item.save()
    else:
        print "price parse failed"


def parsePage(url):
    # print "Extracting", name, "at", url
    not_found = True
    while not_found:
        try:
            page = urllib2.urlopen(url)  # raw html
            results = page.read()
            not_found = False
        except urllib2.HTTPError as err:
            print err
            time.sleep(5)
    raw = BeautifulSoup(results)
    msg = raw.select('div[class=market_listing_table_message]')
    if msg:
        print "Msg:", msg[0].string.strip()
    if len(msg) > 0 and "There are no listings for this item." in msg[0].string:
        return None, None, None, None
    image = getImage(raw)
    quality = getQuality(raw)
    collection = getCollection(raw)
    vig = getVIG(raw)
    return image, quality, collection, vig


def getImage(raw):
    if len(raw.select('div[class=market_listing_largeimage]')) < 1:
        return
    img = raw.select('div[class=market_listing_largeimage]')[0]
    image = img.select('img')[0].get('src')  # image
    image = "/".join(image.split('/')[0:-1]) + "/"  # image
    return image


def getQuality(raw):
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string:
            metadata = script.string
            ty = metadata.lower()

            z = ""
            if '''base grade ''' in ty:
                z = "Base Grade"
            if '''consumer grade ''' in ty:
                z = "Consumer Grade"
            if '''mil-spec grade ''' in ty:
                z = "Mil-Spec Grade"
            if '''industrial grade ''' in ty:
                z = "Industrial Grade"
            if '''restricted ''' in ty:
                z = "Restricted"
            if '''classified ''' in ty:
                z = "Classified"
            if '''high grade ''' in ty:
                z = "High Grade"
            if '''covert ''' in ty:
                z = "Covert"
            if '''remarkable ''' in ty:
                z = "Remarkable"
            if '''exotic ''' in ty:
                z = "Exotic"
            if '''contraband ''' in ty:
                z = "Contraband"

            return z


def getCollection(raw):
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string:
            metadata = script.string
            ty = metadata.lower()

            c = ""
            for coll in collections:
                if coll.lower() in ty.lower():
                    c = coll

            return c


def getVIG(raw):
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string and "inspect in game..." in script.string.lower():
            metadata = script.string
            ty = metadata.lower()
            ty = ty.split("var g_rgassets = ")[1].split("var g_rgcurrency = ")[0].strip()
            ty = ty.replace(";", "")
            #print ty[15805:]
            try:
                ty = demjson.decode(ty, strict=False)
            except KeyboardInterrupt:
                return
            except:
                print "|||", ty[-20:], "|||"
                print url
                return
            owner_steamid = ""
            assetid = ""
            for vsn in ty["730"]:
                for listing in ty["730"][vsn]:
                    owner_steamid = ty["730"][vsn][listing]["owner"]
                    assetid = listing
                    if "actions" in [key for key in ty["730"][vsn][listing]]:
                        if len(ty["730"][vsn][listing]["actions"]) > 0:
                            flag = False
                            if (("name" in [key for key in ty["730"][vsn][listing]["actions"][0]]) and ty["730"][vsn][listing]["actions"][0]["name"] == "inspect in game..."):
                                    flag = True
                            if flag and "link" in [key for key in ty["730"][vsn][listing]["actions"][0]]:
                                z = ty["730"][vsn][listing]["actions"][0]["link"]
                                z = z.replace("s%owner_steamid%", "S" + owner_steamid)
                                z = z.replace("a%assetid%d", "A" + assetid + "D")
                                return z


def CSGOMarketScan():
    TOTAL = 0
    START = time.time()
    # open base url
    try:
        page = urllib2.urlopen(url2)  # raw json
    except Exception:
        time.sleep(60)
        page = urllib2.urlopen(url2)  # raw json
    # find max number of pages
    expected = json.loads(page.read())['total_count']
    maxpages = expected / count
    print expected, "objects expected."
    # iterate over each page, grabbing each result on the page
    for pg in range(maxpages+1):
        time.sleep(1)
        start = pg * count
        url_values = urllib.urlencode({'start': start, 'count': count})
        request = url2 + "?" + url_values
        success = False
        while success is False:
            try:
                respage = urllib2.urlopen(request)  # raw json for results page
                success = True
            except urllib2.HTTPError as err:
                print err
                time.sleep(5)

        html = json.loads(respage.read())['results_html']
        #print html
        raw = BeautifulSoup(html)  # we only need to soup the unescaped json html response
        res = raw.select('a[class=' + item + ']')
        #print raw
        for r in res:
            try:
                extract(r)
                TOTAL = TOTAL + 1
            except AttributeError as err:
                e = Error(key='time', value=datetime.datetime.now(), db=conn)
                e.source = "CSGO Market Listing Scraper"
                e.severity = "Low"
                e.error = str(err)
                e.save()
                print err
                pass
            except IndexError as err:
                e = Error(key='time', value=datetime.datetime.now(), db=conn)
                e.source = "CSGO Market Listing Scraper"
                e.severity = "Low"
                e.error = str(err)
                e.save()
                print err
                pass
            if TOTAL % 10 == 0:
                elapsed = time.time() - START
                eta = elapsed/TOTAL*(expected-TOTAL)
                mins = trunc(eta / 60)
                secs = trunc(eta % 60)
                print TOTAL, "/", expected, '%.3fs' % (elapsed), "ETA:", '%dm:%ds' % (mins, secs)

    print TOTAL, "objects successfully updated."


class CSItem(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    CSGOMarketScan()
