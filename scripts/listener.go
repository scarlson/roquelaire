package main

import (
    //"io"
    "log"
    "net"
    "fmt"
    "strings"
)

const listenAddr = "localhost:55792"

func listen() {
    l, err := net.Listen("tcp", listenAddr)
    if err != nil {
        log.Fatal(err)
    }
    var s string
    for {
        c, err := l.Accept()
        if err != nil {
            log.Fatal(err)
        }
        fmt.Fscan(c, &s)
        s = strings.Replace(s, "*", " ", -1)
        fmt.Println("Received:", s)
        fmt.Fprint(c, s)
    }
}

func main() {
    fmt.Println("Starting listener")
    listen()
    fmt.Println("Listener started.")
}
