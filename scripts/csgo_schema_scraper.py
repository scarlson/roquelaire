#!/usr/bin/env python
import psycopg2
import urllib2
#import itertools
import json
#import datetime
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'

#item detail fields
steam_api_key = "9685698F1B42829AD1024539E8BA196F"
schema = "http://api.steampowered.com/IEconItems_730/GetSchema/v0001/?key=" + steam_api_key + "&format=json&language=en"
url = "https://raw.githubusercontent.com/SteamDatabase/SteamTracking/master/ItemSchema/CounterStrikeGlobalOffensive.json"

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def extract(item, dest):
    updateme = False
    i = CSItem(key='name', value=dest, db=conn)
    if i._exists:
        if i.type != item['item_type_name'].title():
            i.type = item['item_type_name'].title()
            updateme = True
        try:
            if i.description != item["item_description"]:
                i.description = item["item_description"]
                updateme = True
        except:
            pass
        #print i.name, i.base, i.description, i.type
        if updateme:
            i.save()
    else:
        print i.name, "not in the database."


def extractCollections(collections):
    for c in collections:
        coll = CSCollection(key='name', value=c["name"], db=conn)
        coll.id = c["item_set"]
        coll.save()


def extractContainers(items):
    for item in items:
        if item["item_type_name"] == "Container":
            crate = CSCrate(key='name', value=item['item_name'], db=conn)
            crate.id = item["name"]
            try:
                crate.key = item["tool"]["restriction"]
                crate.save()
            except:
                print crate.name, "has no key"


def extractKeys(items):
    for item in items:
        if item["item_type_name"] == "Key":
            key = CSKey(key='name', value=item['item_name'].strip(), db=conn)
            try:
                key.id = item["tool"]["restriction"]
                key.save()
            except:
                print key.name, "has no key"


def CSGOSchemaScan():
    # open game item schema
    print "loading the schema"
    page = urllib2.urlopen(url)  # raw json
    results = json.loads(page.read())['result']
    items = results['items']
    collections = results['item_sets']
    query = "select distinct name, base from csitem;"
    cur.execute(query)
    names = cur.fetchall()
    #names = set(itertools.chain(*names))
    print len(names), "items in database."
    updated = 0
    # iterate through the table and fill in data from an items description page
    for i in items:
        for name in names:
            if i["item_name"].lower() == name[1].lower():
                try:
                    extract(i, name[0])
                    updated = updated + 1
                except KeyboardInterrupt:
                    return
                except Exception as err:
                    print i["item_name"], err
    print updated, "items updated."
    extractCollections(collections)
    extractContainers(items)
    extractKeys(items)


class CSCollection(BaseObject):
    pass


class CSKey(BaseObject):
    pass


class CSCrate(BaseObject):
    #name text
    #id text
    #contents text
    #key text
    pass


class Error(BaseObject):
    pass


class CSItem(BaseObject):
    pass


if __name__ == "__main__":
    CSGOSchemaScan()
