#!/usr/bin/env python
import psycopg2
import urllib2
from base import BaseObject
from kv import keyvalues

DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'

url = "https://raw.githubusercontent.com/SteamDatabase/GameTracking/master/571/dota_english_utf8.txt"

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def parseVDF(f):
    print 'parsing file'
    kv = keyvalues()
    kv.kv = kv.parse(f)
    print 'done'
    return kv.kv['items_game']


def parseAutographs(data):
    ags = data["items_autographs"]
    for auto in ags:
        da = DotaAutograph(key='id', value=auto, db=conn)
        da.name = ags[auto]['name']
        da.autograph = ags[auto]['autograph']
        da.save()


def parseCurrencies(data):
    currency = data["store_currency_pricepoints"]
    for point in currency:
        for k, v in currency[point].iteritems():
            curr = Currency(key='code', value=k, db=conn)
            if curr._exists:
                curr.valve = True
                curr.save()


def parseKillEaters(data):
    killeaters = data["kill_eater_score_types"]
    for ke in killeaters:
        k = DotaKillEater(key='id', value=ke, db=conn)
        k.type = killeaters[ke]['type_name']
        try:
            k.level = killeaters[ke]["level_data"]
        except:
            pass
        k.save()


def parseColors(data):
    colors = data["colors"]
    for color in colors:
        c = DotaColor(key='id', value=color, db=conn)
        c.name = colors[color]['color_name']
        c.color = colors[color]['hex_color']
        c.save()


def parsePlayerSlots(data):
    slots = data["player_loadout_slots"]
    for slot in slots:
        s = DotaPlayerSlot(key='id', value=slot, db=conn)
        s.name = slots[slot]
        s.save()


def parseRarities(data):
    rs = data['rarities']
    for r in rs:
        rare = DotaRarity(key='code', value=r, db=conn)
        rare.color = rs[r]['color']
        rare.name = rs[r]['loc_key'].replace("Rarity_", "")
        rare.lockey = rs[r]['loc_key']
        rare.id = rs[r]['value']
        rare.save()


def parseQualities(data):
    qs = data['qualities']
    for q in qs:
        qual = DotaQuality(key='code', value=q, db=conn)
        qual.color = qs[q]['hexColor']
        qual.save()


def parseLootLists(data):
    ll = data['loot_lists']
    for k in ll:
        contents = []
        for drop in ll[k]:
            contents.append(drop)

        l = DotaLootList(key='name', value=k, db=conn)
        l.contents = contents
        l.save()


def parseSets(data):
    sets = data['item_sets']
    for k in sets:
        contents = []
        for item in sets[k]["items"]:
            contents.append(item)

        s = DotaSet(key='id', value=k, db=conn)
        s.items = contents
        try:
            s.storebundle = sets[k]["store_bundle"]
        except:
            pass

        s.save()


def parseItems(data):
    for k, v in data['items'].iteritems():
        if k != 'default':
            i = DotaBaseItem(key='id', value=k, db=conn)
            i.name = v['name']
            try:
                i.slot = " ".join(v['item_slot'].split("_"))
            except KeyError:
                if v['prefab'] == "wearable":
                    i.slot = "weapon"
                else:
                    i.slot = " ".join(v['prefab'].split("_"))
            if i.slot == 'tool':
                try:
                    i.slot = " ".join(v['tool']['type'].split("_"))
                except KeyError:
                    pass
            try:
                for h in v['used_by_heroes']:
                    if h != '1' and h != '0':
                        i.heroid = h
            except KeyError:
                pass
            try:
                i.creationdate = v['creation_date']
            except KeyError:
                pass
            try:
                i.rarity = v['item_rarity'].title()
            except KeyError:
                pass
            try:
                i.model = v['model_player']
            except KeyError:
                pass
            try:
                i.particle = v['particle_file']
            except KeyError:
                pass
            try:
                i.lootlist = v['static_attributes']['treasure loot list']['value']
            except KeyError:
                pass
            try:
                if v['price_info']['price'] != '0':
                    i.storeprice = float(v['price_info']['price'])/100
                else:
                    i.storeprice = None
            except KeyError:
                pass
            i.save()


def vdfScan():
    print 'opening url'
    dota = urllib2.urlopen(url)
    dota = dota.read()
    print 'done'
    data = parseVDF(dota)

    parseQualities(data)
    parseRarities(data)
    parsePlayerSlots(data)
    parseLootLists(data)
    parseSets(data)
    parseColors(data)
    parseKillEaters(data)
    parseItems(data)
    parseCurrencies(data)
    parseAutographs(data)


class Error(BaseObject):
    pass


class DotaBaseItem(BaseObject):
    pass


class DotaSet(BaseObject):
    pass


class DotaRarity(BaseObject):
    pass


class DotaQuality(BaseObject):
    pass


class DotaLootList(BaseObject):
    pass


class DotaPlayerSlot(BaseObject):
    pass


class DotaColor(BaseObject):
    pass


class DotaKillEater(BaseObject):
    pass


class Currency(BaseObject):
    pass


class DotaAutograph(BaseObject):
    pass


if __name__ == "__main__":
    vdfScan()
