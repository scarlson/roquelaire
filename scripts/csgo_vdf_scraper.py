#!/usr/bin/env python
import psycopg2
import urllib2
from base import BaseObject
from kv import keyvalues

DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'

url = "https://github.com/SteamDatabase/SteamTracking/blob/master/ItemSchema/API/CounterStrikeGlobalOffensive.vdf"


conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

#crates == "client_loot_lists"

"""
Quality
Base Grade
Consumer Grade
Mil-Spec Grade
Industrial Grade
Restricted
Classified
High Grade
Covert
Remarkable
Exotic
Contraband
"""


wpn_codes = {
 "mac10": "MAC-10",
 "m4a1_silencer": "M4A1-S",
 "ssg08": "SSG 08",
 "usp_silencer": "USP-S",
 "Butterfly Knife",
 "g3sg1": "G3SG1",
 "bizon": "PP-Bizon",
 "Karambit",
 "glock": "Glock-18",
 "p250": "P250",
 "m249": "M249",
 "M9 Bayonet",
 "Bayonet",
 "nova": "Nova",
 "fiveseven": "Five-SeveN",
 "negev": "Negev",
 "aug": "AUG",
 "m4": "M4A4",
 "ump45": "UMP-45",
 "Flip Knife",
 "xm1014": "XM1014",
 "mag7": "MAG-7",
 "sawedoff": "Sawed-Off",
 "mp9": "MP9",
 "tec9": "Tec-9",
 "awp": "AWP",
 "cz75a": "CZ75-Auto",
 "galilar": "Galil AR",
 "hkp2000": "P2000",
 "scar20": "SCAR-20",
 "Huntsman Knife",
 "famas": "FAMAS",
 "elite": "Dual Berettas",
 "deagle": "Desert Eagle",
 "p90": "P90",
 "ak47": "AK-47",
 "Gut Knife",
 "sg556": "SG 553",
 "mp7": "MP7",
}


def parseVDF(f):
    kv = keyvalues()
    kv.kv = kv.parse(f)
    items = {}
    for k, v in kv.kv['items_game']['paint_kits_rarity'].iteritems():
        try:
            items[v['name']] = v
        except KeyError:
            raise
    return items


def CSGOvdfScan():
    csgo = urllib2.urlopen(url)
    csgo = csgo.read()
    data = parseVDF(csgo)
    #for k, v in data['items_game'].iteritems():
    #    print k
    query = "select distinct name, base from csitem where slot is null and base is not null;"
    cur.execute(query)
    items = cur.fetchall()
    for item in items:
        i = CSItem(key='name', value=item[0], db=conn)
        if i._exists:
            try:
                i.slot = data[item[1]]
                i.save()
            except KeyError:
                print item[0], "failed."


class Error(BaseObject):
    pass


class CSItem(BaseObject):
    pass


if __name__ == "__main__":
    CSGOvdfScan()
