#!/usr/bin/env python
import psycopg2
import urllib2
import datetime
import time
from bs4 import BeautifulSoup
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

TRADEMARK = u"\u2122"
STAR = u"\u2605"


def extract(name, url):
    # print "Extracting", name, "at", url
    try:
        page = urllib2.urlopen(url)  # raw html
        results = page.read()
    except urllib2.HTTPError:
        time.sleep(1)
        extract(name, url)
        return
    raw = BeautifulSoup(results)  # we only need to soup the unescaped json html response
    metadata = ""

    msg = raw.select('div[class=market_listing_table_message]')
    if msg:
        print "Msg:", msg[0].string.strip()
    if len(msg) > 0 and "There are no listings for this item." in msg[0].string:
        return
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string:
            metadata = script.string
            ty = metadata.lower()

            z = ""
            if '''base grade ''' in ty:
                z = "Base Grade"
            if '''consumer grade ''' in ty:
                z = "Consumer Grade"
            if '''mil-spec grade ''' in ty:
                z = "Mil-Spec Grade"
            if '''industrial grade ''' in ty:
                z = "Industrial Grade"
            if '''restricted ''' in ty:
                z = "Restricted"
            if '''classified ''' in ty:
                z = "Classified"
            if '''high grade ''' in ty:
                z = "High Grade"
            if '''covert ''' in ty:
                z = "Covert"
            if '''remarkable ''' in ty:
                z = "Remarkable"
            if '''exotic ''' in ty:
                z = "Exotic"
            if '''contraband ''' in ty:
                z = "Contraband"

            #if base == na and len(z) > 0:
            if z != "":
                try:
                    i = CSItem(key='name', value=name, db=conn)
                    if i._exists:
                        #print "saving"
                        i.quality = z
                        i.save()
                        break
                    else:
                        print name, "doesn't exist?"
                except:
                    raise
            else:
                print "No quality found for", name


def CSGOQualityScan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct name, link from csitem where quality is null;"
    cur.execute(query)
    names = cur.fetchall()
    print len(names), "items without quality."
    count = 0
    for t in names:
        try:
            #time.sleep(1)
            extract(t[0], t[1])
            count += 1
            #print count, "items updated."
        except KeyboardInterrupt:
            return
        except Exception as err:
            raise
            e = Error(key='time', value=datetime.datetime.now(), db=conn)
            e.source = "CSGO Quality Scraper"
            e.severity = "Low"
            e.error = str(err)
            e.save()


class CSItem(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    CSGOQualityScan()
