#!/usr/bin/env python
import psycopg2
import urllib2
import datetime
import time
from bs4 import BeautifulSoup
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

query = "select distinct name from cscollection;"
cur.execute(query)
colls = cur.fetchall()
collections = []
for c in colls:
    collections.append(c[0])

types = [
    "Shotgun",
    "Sniper Rifle",
    "Smg",
    "Rifle",
    "Pistol",
    "Machinegun"
]


def extract(name, url):
    # print "Extracting", name, "at", url
    try:
        page = urllib2.urlopen(url)  # raw html
        results = page.read()
    except urllib2.HTTPError:
        time.sleep(1)
        extract(name, url)
        return
    raw = BeautifulSoup(results)  # we only need to soup the unescaped json html response
    metadata = ""

    msg = raw.select('div[class=market_listing_table_message]')
    if msg:
        print "Msg:", msg[0].string.strip()
    if len(msg) > 0 and "There are no listings for this item." in msg[0].string:
        return
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string:
            metadata = script.string
            ty = metadata.lower()

            c = ""
            for coll in collections:
                if coll.lower() in ty.lower():
                    c = coll

            #if base == na and len(z) > 0:
            if c != "":
                try:
                    i = CSItem(key='name', value=name, db=conn)
                    if i._exists:
                        i.collection = c
                        i.save()
                        break
                    else:
                        print name, "doesn't exist?"
                except:
                    raise
            else:
                print "No collection found for", name


def CSGOCollectionScan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct name, link from csitem where collection is null and type = ANY(%s);"
    query = cur.mogrify(query, (types,))
    cur.execute(query)
    names = cur.fetchall()
    print len(names), "items without a collection."
    count = 0
    for t in names:
        try:
            #time.sleep(1)
            extract(t[0], t[1])
            count += 1
            #print count, "items updated."
        except KeyboardInterrupt:
            return
        except Exception as err:
            raise
            e = Error(key='time', value=datetime.datetime.now(), db=conn)
            e.source = "CSGO Collection Scraper"
            e.severity = "Low"
            e.error = str(err)
            e.save()


class CSItem(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    CSGOCollectionScan()
