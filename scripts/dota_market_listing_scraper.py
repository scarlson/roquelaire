#!/usr/bin/env python
import psycopg2
import urllib
import urllib2
import json
import time
from bs4 import BeautifulSoup
from base import BaseObject

url = "http://steamcommunity.com/market/search?q=appid%3A570"
url2 = "http://steamcommunity.com/market/search/render/?query=appid%3A570"
pagination = "#p\d+"
pagination2 = "&start=0&count=100"

count = 10
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

# search result fields
item = "market_listing_row_link"
itemimg = "market_listing_row_link img"
listingcount = "market_listing_num_listings_qty"
lowestprice = "br"  # 2nd break trimmed
price = "market_table_value"  # span child
itemname = "market_listing_item_name"
itemgame = "market_listing_game_name"
nextpage = "searchResults_btn_next"
prevpage = "searchResults_btn_prev"
maxpage = "market_paging_pagelink"  # last child
total = "searchResults_total"
#item detail fields
idimg = "market_listing_largeimage img"
idgame = "largeiteminfo_game_name"
idtype = "largeiteminfo_item_type"
iddesc = "largeiteminfo_item_descriptors"

q = "select distinct name from dotaquality;"
cur.execute(q)
quals = cur.fetchall()
qualities = []
for qual in quals:
    qualities.append(qual[0])

r = "select distinct name from dotararity;"
cur.execute(r)
rares = cur.fetchall()
rarities = []
for rare in rares:
    rarities.append(rare[0])

ct = 0


def extract(soup):
    global ct
    ct = ct + 1
    name = soup.select('span[class=' + itemname + ']')[0].string  # name
    if not name:
        return
    quality = name.split(" ")[0]
    item = DotaItem(key="name", value=name, db=conn)
    if quality.lower() in [q.lower() for q in qualities]:
        item.quality = quality
        item.base = " ".join(name.split(" ")[1:])
    else:
        item.quality = "Standard"
        item.base = name
    item.link = soup.get('href')
    item.count = "".join(soup.select('span[class=' + listingcount + ']')[0].string.split(','))
    p = soup.select('span[class=' + price + ']')[0].find('span').text
    if p[0] == "$" and name:
        if not item.rarity or not item.image:
            print "getting rare and image for", name,
            item.rarity, item.image = parsePage(item.link, item.base)
            print item.rarity
        item.price = p[1::]
        item.save()


def ResultGrab(r):
    try:
        extract(r)
    except AttributeError:
        pass
    except IndexError:
        pass


def parsePage(url, base):
    # print "Extracting", name, "at", url
    found = False
    while not found:
        try:
            page = urllib2.urlopen(url)  # raw html
            results = page.read()
            found = True
        except urllib2.HTTPError as err:
            print err
            time.sleep(5)
    raw = BeautifulSoup(results)
    msg = raw.select('div[class=market_listing_table_message]')
    if msg:
        print "Msg:", msg[0].string.strip()
    if len(msg) > 0 and "There are no listings for this item." in msg[0].string:
        return None, None
    q = "select distinct type from dotabaseitem where name like %s;"
    cur.execute(q, (base,))
    try:
        t = cur.fetchone()[0]
        rarity = getRarity(raw, t)
    except:
        return None, None
    image = getImage(raw)
    return rarity, image


def getImage(raw):
    if len(raw.select('div[class=market_listing_largeimage]')) < 1:
        return
    img = raw.select('div[class=market_listing_largeimage]')[0]
    image = img.select('img')[0].get('src')  # image
    image = "/".join(image.split('/')[0:-1]) + "/"  # image
    return image


def getRarity(raw, t):
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string:
            metadata = script.string
            ty = metadata
            for rare in rarities:
                if '"type":"' + rare.lower() + " " + t.lower() in ty.lower():
                    return rare
    return


def PageGrab(page):
    found = False
    start = page * count
    url_values = urllib.urlencode({'start': start, 'count': count})
    request = url2 + "?" + url_values
    try:
        respage = urllib2.urlopen(request)  # raw json for results page
        found = True
    except urllib2.HTTPError:
        found = False

    if found:
        html = json.loads(respage.read())['results_html']
        #print html
        raw = BeautifulSoup(html)  # we only need to soup the unescaped json html response
        res = raw.select('a[class=' + item + ']')
        #print raw
        for r in res:
            ResultGrab(r)
    else:
        print "HTTP error, waiting 5 seconds."
        time.sleep(5)
        PageGrab(page)


def MarketScan():
    TOTAL = 0
    # open base url
    try:
        page = urllib2.urlopen(url2)  # raw json
    except urllib2.HTTPError:
        time.sleep(30)
        MarketScan()
        return
    # find max number of pages
    expected = json.loads(page.read())['total_count']
    maxpages = expected / count
    print expected, "objects expected."
    # iterate over each page, grabbing each result on the page
    try:
        for pg in range(maxpages+1):
            PageGrab(pg)
            time.sleep(1)
            TOTAL = TOTAL + 1
    except KeyboardInterrupt:
        raise
    except TypeError:
        pass
    except:
        pass

    print ct, "objects successfully updated."


class DotaItem(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    MarketScan()
