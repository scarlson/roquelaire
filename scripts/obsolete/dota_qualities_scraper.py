#!/usr/bin/env python
import urllib2
import json
import psycopg2
from base import BaseObject

steam_api_key = "9685698F1B42829AD1024539E8BA196F"
schema = "http://api.steampowered.com/IEconItems_570/GetSchema/v0001/?key=" + steam_api_key + "&format=json&language=en"

DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def SchemaScan():
    # open game item schema
    print "loading the schema"
    page = urllib2.urlopen(schema)  # raw json
    results = json.loads(page.read())['result']
    qualities = results['qualities']
    qualityNames = results['qualityNames']
    for q in qualities:
        qual = DotaQuality(key='id', value=qualities[q], db=conn)
        qual.name = qualityNames[q]
        qual.code = q
        qual.save()


class DotaQuality(BaseObject):
    pass


if __name__ == "__main__":
    SchemaScan()
