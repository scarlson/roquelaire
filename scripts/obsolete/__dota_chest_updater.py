#!/usr/bin/env python
import psycopg2

DATABASE = 'dbname=steammarket user=postgres'
conn = psycopg2.connect('dbname=steammarket user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def qry(query):
    try:
        cur.execute(query)
        conn.commit()
        return True
    except:
        conn.rollback()
        return False


def scan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct name from item;"
    cur.execute(query)
    names = cur.fetchall()
    for name in names:
        name = name[0]
        if "Treasure of" in name:
            chest = Item(key="name", value=name)
            chest.slot = "Chest"
            chest.save()
            print chest.name


class Item(object):
    def __init__(self, value, table=None, key=None, db=None):
        self._table = table or self.__class__.__name__.lower()
        self._key = key or self._table + '_id'
        self._db = db
        self._getVals()
        self._fetch(value)

    def __eq__(self, notself):
        if type(self) == type(notself):
            return self.__dict__[self._key] == notself.__dict__[notself._key]
        return False

    @property
    def conn(self):
        if not hasattr(self, '_conn'):
            if self._db:
                self._conn = self._db
            else:
                self._conn = psycopg2.connect(DATABASE)
                self._conn.set_client_encoding('UTF8')
        return self._conn

    @property
    def cur(self):
        if not hasattr(self, '_cur'):
            self._cur = self.conn.cursor()
        if self._cur.closed:
            self._cur = self.conn.cursor()
        return self._cur

    def select(self, query, *params):
        if params:
            query = self.cur.mogrify(query, params)
        else:
            query = self.cur.mogrify(query)
        self.cur.execute(query)
        res = self.cur.fetchall()
        self.cur.close()
        if res:
            if len(res) > 1:
                return res
            else:
                return res[0]
        return None

    def update(self, query, *params):
        if params:
            query = self.cur.mogrify(query, params)
        else:
            query = self.cur.mogrify(query)
        try:
            self.cur.execute(query)
            self.conn.commit()
            self.cur.close()
            return True
        except:
            raise
            self.conn.rollback()
            self.cur.close()
            return False

    def _getVals(self):
        q = "select * from %s limit 1;" % self._table
        q = self.cur.mogrify(q)
        self.cur.execute(q)
        vals = [val[0].lower() for val in self.cur.description]
        self._vals = vals
        for val in vals:
            self.__dict__[val] = None
        self.cur.close()

    @property
    def _exists(self):
        if not hasattr(self, '__exists'):
            q = 'select * from %(_table)s where %(_key)s =' % self.__dict__
            q += ' %s;'
            try:
                if self.select(q, self.__dict__[self._key]):
                    self.__exists = True
                else:
                    self.__exists = False
            except:
                self.conn.rollback()
                self.__exists = False
        return self.__exists

    def _fetch(self, unid):
        if hasattr(self, '_key') and hasattr(self, '_table'):
            q = 'select * from %(_table)s where %(_key)s =' % self.__dict__
            q += ' %s;'
            try:
                vals = self.select(q, unid)
            except:
                vals = None
                self.conn.rollback()
            if vals:
                vals = list(vals[::-1])
                for val in self._vals:
                    self.__dict__[val] = vals.pop()
            else:
                for val in self._vals:
                    self.__dict__[val] = None
                self.__dict__[self._key] = unid

    def save(self):
        if hasattr(self, '_key') and hasattr(self, '_table'):
            if self._exists:
                keys = tuple(val for val in self._vals if val != self._key)
                values = tuple(self.__dict__[val] for val in self._vals if val != self._key)
                q = 'update ' + self._table + ' set (' + ", ".join(keys) + ') = %s where ' + self._key + ' = %s;'
                return self.update(q, values, self.__dict__[self._key])
            else:
                keys = tuple(val for val in self._vals)
                values = tuple(self.__dict__[val] for val in self._vals)
                q = "insert into " + self._table + ' (' + ", ".join(keys) + ") values %s;"
                return self.update(q, values)
        return False


if __name__ == "__main__":
    scan()
