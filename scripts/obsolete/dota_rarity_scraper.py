#!/usr/bin/env python
import psycopg2
import urllib2
import datetime
import time
from bs4 import BeautifulSoup
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def extract(name, base, url):
    page = urllib2.urlopen(url)  # raw html
    results = page.read()
    raw = BeautifulSoup(results)  # we only need to soup the unescaped json html response
    metadata = ""
    msg = raw.select('div[class=market_listing_table_message]')
    if msg:
        print "Msg:", msg[0].string.strip()
    if len(msg) > 0 and "There are no listings for this item." in msg[0].string:
        return
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string:
            metadata = script.string
            ty = metadata.lower()
            z = ""
            if '''"type":"common ''' in ty:
                z = "Common"
            if '''"type":"uncommon ''' in ty:
                z = "Uncommon"
            if '''"type":"rare ''' in ty:
                z = "Rare"
            if '''"type":"mythical ''' in ty:
                z = "Mythical"
            if '''"type":"legendary ''' in ty:
                z = "Legendary"
            if '''"type":"ancient ''' in ty:
                z = "Ancient"
            if '''"type":"immortal ''' in ty:
                z = "Immortal"
            if '''"type":"arcana ''' in ty:
                z = "Arcana"
            #if base == na and len(z) > 0:
            if z != "":
                try:
                    i = DotaItem(key='name', value=name, db=conn)
                    if i._exists:
                        i.rarity = z
                        i.save()
                        break
                except:
                    pass
            else:
                print "No rarity found for", name


def RarityScan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct name, base, link from dotaitem where rarity is null order by base;"
    cur.execute(query)
    names = cur.fetchall()
    print len(names), "items without rarity."
    count = 0
    for t in names:
        query = "select distinct link from dotaitem where name like %s;"
        q = cur.mogrify(query, (t[1],))
        cur.execute(q)
        url = cur.fetchall()
        if len(url) == 0:
            url = [(t[2],)]
        try:
            time.sleep(1)
            if t[1]:
                extract(t[0], t[1], url[0][0])
            else:
                extract(t[0], t[0], url[0][0])
            count += 1
            print count, "items updated."
        except KeyboardInterrupt:
            break
        except Exception as err:
            e = Error(key='time', value=datetime.datetime.now(), db=conn)
            e.source = "Rarity Scraper"
            e.severity = "Low"
            e.error = str(err)
            e.save()


class DotaItem(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    RarityScan()
