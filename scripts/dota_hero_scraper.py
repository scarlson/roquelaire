#!/usr/bin/env python
import psycopg2
import urllib2
import json
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields
steam_api_key = "9685698F1B42829AD1024539E8BA196F"
herourl = "http://api.steampowered.com/IEconDOTA2_570/GetHeroes/v1/?format=json&language=en&key=" + steam_api_key


conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def HeroScan():
    print "loading the hero schema"
    page = urllib2.urlopen(herourl)  # raw json
    results = json.loads(page.read())['result']
    print "done"
    heroes = results['heroes']
    for hero in heroes:
        h = DotaHero(key='heroid', value=hero["name"], db=conn)
        h.id = hero["id"]
        h.name = hero["localized_name"]
        h.image = "/img/heroes/" + hero["name"].replace("npc_dota_hero_", "") + "_full.png"
        h.save()


class Error(BaseObject):
    pass


class DotaHero(BaseObject):
    pass


if __name__ == "__main__":
    HeroScan()
