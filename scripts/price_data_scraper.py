#!/usr/bin/env python
import psycopg2
import urllib2
import json
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'

"""
              Table "public.price"
   Column    |         Type          | Modifiers
-------------+-----------------------+----------
 rate        | character varying(20) |
 code        | character varying(3)  |
 description | text                  |
 symbol      | character varying(1)  |
 separator   | character varying(1)  |
 valveid      | varchar(4)            |
"""


conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

OpenExchangeRatesApiKey = "14fbc4eea5574945a1501310ece9f3e4"
OERUrl = "https://openexchangerates.org/api/latest.json?app_id=14fbc4eea5574945a1501310ece9f3e4"
CurNames = "https://openexchangerates.org/api/currencies.json?app_id=14fbc4eea5574945a1501310ece9f3e4"


def extract():
    req = urllib2.Request(CurNames)
    page = urllib2.urlopen(req)  # raw html
    results = page.read()
    js = json.loads(results)
    for k, v in js.iteritems():
        price = Currency(key="code", value=k, db=conn)
        price.code = k
        price.description = v
        price.save()

    req = urllib2.Request(OERUrl)
    page = urllib2.urlopen(req)  # raw html
    results = page.read()
    js = json.loads(results)
    rates = js["rates"]
    for k, v in rates.iteritems():
        price = Currency(key="code", value=k, db=conn)
        price.rate = v
        price.save()


def PriceScan():
    extract()


class Currency(BaseObject):
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    PriceScan()
