#!/usr/bin/env python
from __future__ import division
import psycopg2
import urllib2
#from bs4 import BeautifulSoup
import string
import json
import time
import socket
import datetime
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'

url = "http://steamcommunity.com/market/recentcompleted?language=en&l=english"

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

'''
keys

last_listing      -- last listid provided
assets            -- item names and descriptions by appid?
success           -- whether or not the query was successful
hovers            -- javascript for making popup info
purchaseinfo      -- actual sale record
currency          -- this is a blank array
app_data          -- appids, names, links for apps in the listings
results_html      -- raw html, not really usable
more              -- boolean, what does it mean?
last_time         -- ?? assumable a unix epoch, we should check and make sure we get a new one
listinginfo       -- listing metadata
'''

LASTLISTING = 0  # last_listing, so if there's no new results we can bail
LASTTIME = 0


OpenExchangeRatesApiKey = "14fbc4eea5574945a1501310ece9f3e4"
OERUrl = "https://openexchangerates.org/api/latest.json?app_id=14fbc4eea5574945a1501310ece9f3e4"
CurNames = "https://openexchangerates.org/api/currencies.json?app_id=14fbc4eea5574945a1501310ece9f3e4"


PRICEAPI = {}  # everything = 1USD
LASTPRICE = 0

CURRENCYID = {
    "2001": "USD",
    "2003": "EUR",
    #"GBP": "2005",  # ...2003?
    "2005": "RUB",  # ...really?
    "2007": "BRL",
}

PRICE = "market_listing_price_with_fee"
NAME = "market_listing_item_name"
GAME = "market_listing_game_name"  # = "Dota 2"
ROWS = "market_listing_row"

HOST = ''
PORT = 55792


def update_rates():
    global PRICEAPI
    global LASTPRICE
    prices = {}
    l = ["USD", "EUR", "RUB", "BRL"]
    for i in l:
        prices[i] = Currency(key='code', value=i, db=conn)
    LASTPRICE = time.time()
    PRICEAPI = prices


def send(item):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    i = json.dumps(item)
    derp = json.dumps({"type": "price", "body": i})
    derp = string.replace(derp, " ", "*")
    s.send(derp + " ")
    print "sent:", item
    data = s.recv(1024)
    s.close()
    print "rcvd:", repr(data)


def extract():
    global LASTTIME
    global LASTLISTING
    req = urllib2.Request(url)
    req.add_header("Accept-Language", "en-US,en;q=0.8")
    req.add_header("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36")
    req.add_header("Cookie", "Steam_Language=english;")
    page = urllib2.urlopen(req)  # raw html
    results = page.read()
    js = json.loads(results)
    if js["last_time"] > LASTTIME:
        LASTTIME = js["last_time"]
    else:
        return
    if js["last_listing"] > LASTLISTING:
        LASTLISTING = js["last_listing"]
    else:
        return
    if not js["success"]:
        return

    items = {}
    for k in js["assets"]["570"]["2"]:
        items[k] = {"name": js["assets"]["570"]["2"][k]["name"]}
    purchases = js["purchaseinfo"]
    for item in items:
        for k in purchases:
            if purchases[k]["asset"]["id"] == item:
                if purchases[k]["currencyid"] == "2001":
                    price = purchases[k]["paid_amount"] + purchases[k]["paid_fee"]
                    items[item]["price"] = "{0:.2f}".format(price * 0.01)
                    #print items[item]
                    i = DotaItem(key='name', value=items[item]["name"], db=conn)
                    if i._exists:
                        i.price = items[item]["price"]
                        i.save()
                        send(items[item])
                elif purchases[k]["currencyid"] in ["2003", "2005", "2007"]:
                    price = (purchases[k]["paid_amount"] + purchases[k]["paid_fee"]) / 100
                    #print "Price:", price, CURRENCYID[purchases[k]["currencyid"]]
                    price = round(price / PRICEAPI[CURRENCYID[purchases[k]["currencyid"]]].rate, 2)
                    if price < 0.03:
                        price = 0.03
                    items[item]["price"] = "{0:.2f}".format(price)
                    #print "Check:", items[item]["name"], "=", price
                    i = DotaItem(key='name', value=items[item]["name"], db=conn)
                    if i._exists:
                        i.price = items[item]["price"]
                        i.save()
                        send(items[item])


def PriceScan():
    #timeout = time.time() + 60*60
    while True:
        if LASTPRICE + 60*60 <= time.time():
            update_rates()
        time.sleep(1)
        try:
            extract()
        except Exception as err:
            raise
            e = Error(key='time', value=datetime.datetime.now(), db=conn)
            e.source = "Price Scraper"
            e.severity = "Low"
            e.error = str(err)
            e.save()
        #if time.time() > timeout:
        #    break


class DotaItem(BaseObject):
    pass


class Error(BaseObject):
    pass


class Currency(BaseObject):
    pass


if __name__ == "__main__":
    update_rates()
    PriceScan()
