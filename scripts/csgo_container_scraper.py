#!/usr/bin/env python
import psycopg2
import urllib2
import time
import demjson
from bs4 import BeautifulSoup
from base import BaseObject
DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
#item detail fields

conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

TRADEMARK = u"\u2122"
STAR = u"\u2605"

types = [
    "Shotgun",
    "Knife",
    "Sniper Rifle",
    "Smg",
    "Rifle",
    "Pistol",
    "Machinegun"
]

'''
{
"currency":0,
"appid":730,
"contextid":"2",
"id":"416803149",
"classid":"469432309",
"instanceid":"0",
"amount":"1",
"status":2,
"background_color":"",
"icon_url":"fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEgznuShMhvflDOGJG68Didsh4K9GzT9omgRpC_O0MzxYcQvPDLVQVeEjpFmiUXFmvpRgAI-3oeJWKgW74oLONrMpMdgdSpLZDPKFZA74uBppgvVZJovJ_n0ueGyl7Q",
"icon_url_large":"",
"descriptions":[
    {"type":"html","value":"This capsule contains a single community-designed sticker. A portion of the proceeds is shared equally among the included creators."},
    {"type":"html","value":" "},
    {"type":"html","value":"Container Series #16","color":"99ccff"},
    {"type":"html","value":" "},
    {"type":"html","value":"Contains one of the following:"},
    {"type":"html","value":"Backstab","color":"4b69ff"},
    {"type":"html","value":"Pocket BBQ","color":"4b69ff"},
    {"type":"html","value":"Bomb Doge","color":"4b69ff"},
    {"type":"html","value":"Burn Them All","color":"4b69ff"},
    {"type":"html","value":"Llama Cannon","color":"4b69ff"},
    {"type":"html","value":"My Other Awp","color":"4b69ff"},
    {"type":"html","value":"Shave Master","color":"4b69ff"},
    {"type":"html","value":"Rising Skull","color":"4b69ff"},
    {"type":"html","value":"Sneaky Beaky Like","color":"4b69ff"},
    {"type":"html","value":"To B or not to B","color":"4b69ff"},
    {"type":"html","value":"Death Comes","color":"4b69ff"},
    {"type":"html","value":"Teamwork (Holo)","color":"8847ff"},
    {"type":"html","value":"Rekt (Holo)","color":"8847ff"},
    {"type":"html","value":"Headhunter (Foil)","color":"d32ce6"},
    {"type":"html","value":"Flammable (Foil)","color":"d32ce6"},
    {"type":"html","value":"New Sheriff (Foil)","color":"d32ce6"},
    {"type":"html","value":"Swag (Foil)","color":"d32ce6"},
    {"type":"html","value":" "},
    {"type":"html","value":"","color":"00a000"}
],
"tradable":1,
"name":"Community Sticker Capsule 1",
"name_color":"D2D2D2",
"type":"Base Grade Container",
"market_name":"Community Sticker Capsule 1",
"market_hash_name":"Community Sticker Capsule 1",
"commodity":1,
"owner":"76561198088575159"
}
'''


def extract(name, url):
    # print "Extracting", name, "at", url
    try:
        page = urllib2.urlopen(url)  # raw html
        results = page.read()
    except urllib2.HTTPError:
        time.sleep(1)
        extract(name, url)
        return
    raw = BeautifulSoup(results)  # we only need to soup the unescaped json html response

    msg = raw.select('div[class=market_listing_table_message]')
    if len(msg) > 0 and msg[0].string:
        print "Msg:", msg[0].string.strip()
        return
    for script in raw.find_all('script'):
        if script.string and "var g_rgAssets" in script.string:
            metadata = script.string
            ty = metadata
            ty = ty.split("var g_rgAssets = ")[1].split(";")[0].strip()
            ty = ty.replace(";", "").strip()
            #print ty[15805:]
            try:
                ty = demjson.decode(ty, strict=False)
            except KeyboardInterrupt:
                return
            except:
                print "|||", ty[-20:], "|||"
                print url
                raise
                return
            print "\n", name, "contains:\n"
            for vsn in ty["730"]:
                for listing in ty["730"][vsn]:
                    descs = ty["730"][vsn][listing]["descriptions"]
                    ardesc = []
                    start = False
                    stop = False
                    stickers = False
                    for d in descs:
                        if "sticker" in d["value"].lower():
                            stickers = True
                        if "or an Exceedingly Rare" in d["value"]:
                            stop = True
                        if start and not stop and len(d["value"].strip()) > 0:
                            #if stickers:
                            #    ardesc.append("Sticker | " + d['value'].strip())
                            #else:
                            ardesc.append(d['value'].strip())
                        if "Contains " in d['value'] and ":" in d['value']:
                            start = True
                    contents = ",".join(list(set(ardesc)))
                    if len(ardesc) > 0:
                        i = CSCrate(key='name', value=name.strip(), db=conn)
                        #print "saving"
                        i.contents = contents
                        print i.contents
                        i.save()
                        break


def CSGOContainerScan():
    # iterate through the table and fill in data from an items description page
    query = "select distinct csitem.name, csitem.link from csitem where type like 'Container';"
    cur.execute(query)
    names = cur.fetchall()
    print len(names), "containers."
    count = 0
    for t in names:
        try:
            #time.sleep(1)
            extract(t[0], t[1])
            count += 1
            #print count, "items updated."
        except KeyboardInterrupt:
            return
        except ValueError as err:
            print err
        except Exception as err:
            raise
    print len(names), "expected", count, "items updated."


class CSCrate(BaseObject):
    #name text
    #id text
    #contents text
    #key text
    pass


class Error(BaseObject):
    pass


if __name__ == "__main__":
    CSGOContainerScan()
