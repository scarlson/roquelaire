#!/usr/bin/env python
import datetime
import psycopg2
from scripts.base import BaseObject
from scripts.csgo_schema_scraper import CSGOSchemaScan
from scripts.csgo_collection_scraper import CSGOCollectionScan
from scripts.csgo_market_scraper import CSGOMarketScan
from scripts.csgo_ingame_scraper import CSGOInGameScan
from scripts.csgo_image_scraper import CSGOImageScan
from scripts.csgo_container_scraper import CSGOContainerScan


DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')

HOST = ''
PORT = 55792


class Updates(BaseObject):
    pass


if __name__ == "__main__":
    Ups = Updates(key='start', value=None, db=conn)
    Ups.start = datetime.datetime.now()
    CSGOMarketScan()
    CSGOSchemaScan()
    CSGOImageScan()
    CSGOCollectionScan()
    CSGOInGameScan()
    CSGOContainerScan()
    Ups.finish = datetime.datetime.now()
    Ups.type = "CSGO"
    Ups.save()
