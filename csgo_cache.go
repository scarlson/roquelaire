package main

import (
	"database/sql"
	"fmt"
	"strings"
	"time"
)

var imgWidth = "405f"  //"450f"
var imgHeight = "270f" //"300f"

// cache vars
var GunCache = make(map[string][]*CSItem)
var BoxCache = make(map[string]*CSContainer)
var KeyCache = make(map[string]*CSKey)
var SkinCache = make(map[string][]*GunBucket)
var TypeCache = make(map[string]*Type)
var BaseCache = make(map[string]*Base)

var GunBases = make(map[string][]string)
var Collections = make(map[string]bool)
var Types = make(map[string]bool)
var Qualities = make(map[string]bool)
var Skins = make(map[string]map[string]bool)
var Exteriors = make(map[string]bool)

var AcceptableTypes = []string{
	"Shotgun",
	"Sniper Rifle",
	"Smg",
	"Rifle",
	"Pistol",
	"Machinegun",
	"Knife",
	"Sticker",
}

func CacheCSGO() {
	CacheGuns()
	CacheBoxes()
	CacheKeys()
	CacheSkins()
	CacheTypes()
	CacheBases()
}

func CacheTypes() {
	var TC = make(map[string]*Type)
	for ty, _ := range Types {
		TC[ty] = &Type{Name: ty}
	}
	TypeCache = TC
}

func CacheBases() {
	var BC = make(map[string]*Base)
	for _, bases := range GunBases {
		for _, base := range bases {
			t := ""
			for _, guns := range GunCache {
				for _, gun := range guns {
					if gun.Base == base {
						t = gun.Type
					}
				}

			}
			BC[base] = &Base{Name: base, Type: t}
		}
	}
	BaseCache = BC
}

func CacheGuns() {
	var GC = make(map[string][]*CSItem)
	var GB = make(map[string][]string)
	guns, er := db.Query("select base, exterior, quality, type, name, price, description, link, stattrak, souvenir, skin, star, added, collection, image, ingame from csitem;")
	if er != nil {
		fmt.Println(er)
		return
	}
	fmt.Println("Caching guns.")
	var base sql.NullString
	var exterior sql.NullString
	var quality sql.NullString
	var Type sql.NullString
	var name sql.NullString
	var price sql.NullString
	var description sql.NullString
	var link sql.NullString
	var stattrak sql.NullBool
	var souvenir sql.NullBool
	var skin sql.NullString
	var star sql.NullBool
	var added time.Time
	var collection sql.NullString
	var image sql.NullString
	var ingame sql.NullString
	count := 0

	for guns.Next() {
		count = count + 1
		err := guns.Scan(
			&base,
			&exterior,
			&quality,
			&Type,
			&name,
			&price,
			&description,
			&link,
			&stattrak,
			&souvenir,
			&skin,
			&star,
			&added,
			&collection,
			&image,
			&ingame,
		)
		if err != nil {
			fmt.Println(err)
		}
		guns := GC[strings.ToLower(base.String)]
		if image.String != "" {
			guns = append(guns, &CSItem{
				Base:        base.String,
				Exterior:    exterior.String,
				Quality:     quality.String,
				Type:        Type.String,
				Name:        name.String,
				price:       price.String,
				Description: description.String,
				Link:        link.String,
				Stattrak:    stattrak.Bool,
				Souvenir:    souvenir.Bool,
				Skin:        skin.String,
				Star:        star.Bool,
				Added:       added,
				Collection:  collection.String,
				image:       image.String + imgWidth + "x" + imgHeight,
				InGame:      ingame.String,
			})
			GC[strings.ToLower(base.String)] = guns
			if _, ok := GB[strings.ToLower(Type.String)]; !ok {
				GB[strings.ToLower(Type.String)] = []string{}
			}
			bfound := false
			for _, b := range GB[strings.ToLower(Type.String)] {
				if b == base.String {
					bfound = true
					break
				}
			}
			if !bfound {
				GB[strings.ToLower(Type.String)] = append(GB[strings.ToLower(Type.String)], base.String)
			}
			if collection.String != "" {
				Collections[collection.String] = true
			}
			if skin.String != "" {
				flag := false
				for _, t := range AcceptableTypes {
					if Type.String == t {
						flag = true
					}
				}
				if flag == true {
					if _, ok := Skins[strings.ToLower(skin.String)]; !ok {
						Skins[strings.ToLower(skin.String)] = make(map[string]bool)
					}
					Skins[strings.ToLower(skin.String)][strings.ToLower(base.String)] = true
				}
			}
			if exterior.String != "" {
				Exteriors[exterior.String] = true
			}
			if Type.String != "" {
				Types[Type.String] = true
			}
			if quality.String != "" {
				Qualities[quality.String] = true
			}
		}
	}
	GunCache = GC
	GunBases = GB
	fmt.Println(count, "Guns cached.")
}

func CacheKeys() {
	var KC = make(map[string]*CSKey)
	keys, er := db.Query("select name, id from cskey;")
	if er != nil {
		fmt.Println(er)
		return
	}
	fmt.Println("Caching keys.")
	var name sql.NullString
	var id sql.NullString
	count := 0

	for keys.Next() {
		count = count + 1
		err := keys.Scan(
			&name,
			&id,
		)
		if err != nil {
			fmt.Println(err)
		}

		containers := []string{}

		for _, box := range BoxCache {
			if box.keystring == id.String {
				containers = append(containers, box.Name)
			}
		}

		key := &CSKey{
			Name:             name.String,
			Id:               id.String,
			containersstring: containers,
		}
		KC[strings.ToLower(name.String)] = key
		KC[strings.ToLower(id.String)] = key
	}
	KeyCache = KC
	fmt.Println(count, "keys cached.")
}

func CacheBoxes() {
	var BC = make(map[string]*CSContainer)
	boxes, er := db.Query("select name, id, contents, key from cscrate;")
	if er != nil {
		fmt.Println(er)
		return
	}
	fmt.Println("Caching boxes.")
	var name sql.NullString
	var id sql.NullString
	var contents sql.NullString
	var key sql.NullString
	count := 0

	for boxes.Next() {
		count = count + 1
		err := boxes.Scan(
			&name,
			&id,
			&contents,
			&key,
		)
		if err != nil {
			fmt.Println(err)
		}

		// figure out what the hell is in the box, link to those items
		buckets := []*GunBucket{}
		//fmt.Println(name.String)
		collection := ""
		for _, gun := range strings.Split(contents.String, ",") {
			if gun != "" {
				bucket := ParseContainerContents(gun)
				if bucket.Collection != "" {
					collection = bucket.Collection
				}
				if len(bucket.Guns) > 0 {
					addme := true
					for _, b := range buckets {
						if b.Base == bucket.Base && b.Skin == bucket.Skin {
							addme = false
						}
					}
					if addme == true {
						buckets = append(buckets, bucket)
					}
				}
			}
		}

		BC[strings.ToLower(name.String)] = &CSContainer{
			Name:       name.String,
			Id:         id.String,
			keystring:  key.String,
			Contents:   contents.String,
			Guns:       buckets,
			Collection: collection,
		}
	}
	BoxCache = BC
	fmt.Println(count, "boxes cached.")
}

func CacheSkins() {
	var SC = make(map[string][]*GunBucket)
	for skin, _ := range Skins {
		SC[strings.ToLower(skin)] = GetSkinBuckets(skin)
	}
	SkinCache = SC
}
