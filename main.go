package main

import (
	"code.google.com/p/go.net/websocket"
	"database/sql"
	"flag"
	"fmt"
	"github.com/hoisie/mustache"
	"github.com/hoisie/web"
	_ "github.com/lib/pq"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

/* ----------------------------------------------------------------------------
                           START OF DECLARES
-----------------------------------------------------------------------------*/

const listenAddr = "localhost:55792"

var myPid = os.Getpid()

var listening = false
var port = flag.Int("port", 20000, "Port to run the server on")

var priceHub = &Room{
	Broadcast: make(chan string),
	Sockets:   make(map[*Socket]bool),
	Open:      make(chan *Socket),
	Close:     make(chan *Socket),
	Running:   false,
}

var MINPRICE = "0.00"
var MAXPRICE = "400.00"
var HEROES = []string{}
var RARITIES = []string{"Common", "Uncommon", "Rare", "Mythical", "Legendary", "Ancient", "Immortal", "Arcana"}
var QUALITIES = []string{
	"Genuine",
	"Elder",
	"Unusual",
	"Standard",
	//"Community",
	//"Valve",
	//"Self-Made",
	//"Customized",
	"Inscribed",
	//"Completed",
	"Cursed",
	"Heroic",
	//"Favored",
	//"Ascendant",
	"Autographed",
	"Legacy",
	"Exalted",
	"Frozen",
	"Corrupted",
	"Auspicious",
}

// database vars
var (
	databaseName = "steammarket"
	databaseUser = "postgres"
	db, _        = sql.Open("postgres", "host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls sslmode=disable port=5433")
)

type Cmd struct {
	Type string `json:"type"`
	Body string `json:"body"`
}

type Room struct {
	Sockets   map[*Socket]bool
	Broadcast chan string
	Open      chan *Socket
	Close     chan *Socket
	Running   bool
}

func (r *Room) run() {
	fmt.Println("starting the Socket Room up!")
	r.Running = true
	for {
		select {
		case s := <-r.Open:
			r.Sockets[s] = true
		case s := <-r.Close:
			delete(r.Sockets, s)
			close(s.Send)
		case m := <-r.Broadcast:
			//fmt.Println("Broadcasting:", m)
			for s := range r.Sockets {
				select {
				case s.Send <- m:
				default:
					delete(r.Sockets, s)
					close(s.Send)
					go s.Close()
				}
			}
		}
	}
}

type Socket struct {
	Ws   *websocket.Conn
	Send chan string
}

func (s *Socket) Read(output chan string) {
	defer s.Close()
	for {
		var unmarshal []byte
		err := websocket.Message.Receive(s.Ws, &unmarshal)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println("Unmarshal:", string(unmarshal))
	}
}

func (s *Socket) Write() {
	for message := range s.Send {
		err := websocket.Message.Send(s.Ws, message)
		if err != nil {
			break
		}
	}
	s.Close()
}

func (s *Socket) Close() {
	fmt.Println("Closing Socket")
	s.Ws.Close()
}

/* ----------------------------------------------------------------------------
                           END OF DECLARES
-----------------------------------------------------------------------------*/

/* ----------------------------------------------------------------------------
                           START OF HELPER FUNCS
-----------------------------------------------------------------------------*/

func SearchAndDestroy() {
	mypid := os.Getpid()
	out, err := exec.Command("/bin/ps", "aux").Output()
	if err != nil {
		fmt.Println(err)
	}
	pids := []string{}
	outlines := strings.Split(string(out), "\n")
	for _, line := range outlines {
		for i := 0; i <= 10; i++ {
			line = strings.Replace(line, "  ", " ", -1)
		}
		fields := strings.Split(line, " ")
		if len(fields) > 1 {
			process := strings.Join(fields[10:], " ")
			pid := fields[1]
			if *port == 20000 {
				if strings.Contains(process, "/steammarket") {
					pids = append(pids, pid)
				}
			} else if strings.Contains(process, "steammarket -port=15000") {
				pids = append(pids, pid)
			}
		}
	}
	if len(pids) > 0 {
		for _, pid := range pids {
			otherpid, _ := strconv.Atoi(pid)
			if otherpid != mypid {
				other, _ := os.FindProcess(otherpid)
				other.Kill()
			}
		}
	}
}

func setCurrencyCookie(ctx *web.Context, curid string) {
	if cur, ok := CurCache[strings.ToLower(curid)]; ok {
		//fmt.Println("Setting Cur cookie to", curid)
		//fmt.Println("Request:", ctx.Request)
		c := web.NewCookie("cur", cur.Code, 0)
		c.Path = "/"
		ctx.SetCookie(c)
	}
	return
}

func getCurrencyCookie(ctx *web.Context) (*http.Cookie, error) {
	return ctx.Request.Cookie("cur")
}

func setSortCookie(ctx *web.Context, sort string) {
	c := web.NewCookie("cur", sort, 0)
	c.Path = "/"
	ctx.SetCookie(c)
}

func getSortCookie(ctx *web.Context) (*http.Cookie, error) {
	return ctx.Request.Cookie("sort")
}

func getUserCurrency(ctx *web.Context) *Currency {
	cur, err := getCurrencyCookie(ctx)
	if err != nil {
		return CurCache["usd"]
	}
	return CurCache[strings.ToLower(cur.Value)]
}

func getSortMethod(ctx *web.Context) string {
	cur, _ := getSortCookie(ctx)
	return cur.Value
}

func LogError(e error, source, severity string) {
	// error.save()......
	err := &Err{}
	err.E = e
	err.Time = time.Now()
	err.Source = source
	err.Severity = severity
	err.Save()
}

func afixString(s string) string {
	return strings.Replace(strings.Title(s), "'S", "'s", -1)
}

func init() {
	flag.Parse()
	if *port == 20000 {
		if !priceHub.Running {
			go priceHub.run()
		}
		go netListener()
	}
	CacheCurs()
	CurCSS()
	CurJS()
	CacheDota()
	CacheCSGO()
}

/* ----------------------------------------------------------------------------
                           START OF HANDLERS
-----------------------------------------------------------------------------*/

func indexGet(ctx *web.Context) string {
	data := make(map[string]interface{})
	data["index"] = true
	params := ctx.Params
	redir := params["redirect"]
	if redir == "" {
		return mustache.RenderFileInLayout("templates/index.html", "templates/base.html", data)
	}
	ctx.Redirect(302, redir)
	return ""
}

func socketHandler(ws *websocket.Conn) {
	s := &Socket{ws, make(chan string, 256)}
	defer func() { priceHub.Close <- s }()
	priceHub.Open <- s
	go s.Write()
	s.Read(priceHub.Broadcast)
}

func testGet(ctx *web.Context) string {
	data := make(map[string]interface{})
	data["heroes"] = HeroCache
	data["slots"] = SlotCache
	return mustache.RenderFileInLayout("templates/test.html", "templates/base.html", data)
}

func CatchAllHandler(ctx *web.Context, blah string) {
	ctx.Redirect(302, "/")
}

/* ----------------------------------------------------------------------------
                            END OF HANDLERS
-----------------------------------------------------------------------------*/

func main() {
	fmt.Println("Running on port", *port, "as process", myPid)

	web.Config.CookieSecret = "a3wlefkja3LKJaflk"
	web.Config.RecoverPanic = true

	// generic routes
	web.Get("/", indexGet)
	web.Get("/test", testGet)
	web.Get("/pricestream", priceStreamGet)
	web.Websocket("/stream", websocket.Handler(socketHandler))

	// dota routes
	web.Get("/dota", dotaGet)
	web.Get("/dota/heroes", heroesGet)
	web.Get("/dota/search", searchGet)
	web.Get("/dota/player", miscsGet)
	web.Get("/dota/player/(.*)", miscGet)
	web.Get("/dota/hero/(.*)/(.*)/(.*)", heroItemGet)
	web.Get("/dota/hero/(.*)/(.*)", heroSlotGet)
	web.Get("/dota/hero/(.*)", heroGet)
	web.Get("/dota/item/(.*)", itemGet)
	web.Get("/dota/set/(.*)", setGet)
	web.Get("/dota(.*)", DotA404Handler)

	// csgo routes
	web.Get("/csgo", csgoGet)
	//web.Get("/csgo/skins", skinsHandler)
	web.Get("/csgo/gun/(.*)/(.*)", gunHandler)
	web.Get("/csgo/gun/(.*)", baseHandler)
	web.Get("/csgo/qualities", qualitiesHandler)
	web.Get("/csgo/quality/(.*)", qualityHandler)
	web.Get("/csgo/collections", collectionsHandler)
	web.Get("/csgo/collection/(.*)", collectionHandler)
	web.Get("/csgo/skin/(.*)", skinHandler)
	web.Get("/csgo/types", typesHandler)
	web.Get("/csgo/type/(.*)", typeHandler)
	web.Get("/csgo/search", searchHandler)
	web.Get("/csgo(.*)", CSGO404Handler)

	// 404 catchall
	web.Get("/(.*)", CatchAllHandler)

	// we're ready, run it
	SearchAndDestroy()
	time.Sleep(250 * time.Millisecond)
	web.Run(fmt.Sprintf("0.0.0.0:%d", *port))
}
