#!/usr/bin/env python
from scripts.dota_hero_scraper import HeroScan
from scripts.dota_market_listing_scraper import MarketScan
from scripts.dota_schema_scraper import SchemaScan
from scripts.dota_vdf_scraper import vdfScan
from scripts.base import BaseObject
import datetime
import psycopg2
import socket
import json
import string


DATABASE = 'host=localhost dbname=steammarket user=postgres password=squidhumpingpterodactyls port=5433'
conn = psycopg2.connect(DATABASE)
conn.set_client_encoding('UTF8')

HOST = ''
PORT = 55792


class Updates(BaseObject):
    pass


def update_web():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    derp = json.dumps({"type": "update", "body": "4h"})
    derp = string.replace(derp, " ", "*")
    print "sending", derp
    s.send(derp + " ")
    s.close()


if __name__ == "__main__":
    try:
        Ups = Updates(key='start', value=None, db=conn)
        Ups.start = datetime.datetime.now()
        Ups.type = "Dota"
        Ups.status = "starting"
        Ups.save()
        HeroScan()
        Ups.status = "HeroScan done, Schema next"
        Ups.save()
        SchemaScan()
        Ups.status = "SchemaScan done, VDF next"
        Ups.save()
        vdfScan()
        Ups.status = "VDFScan done, Market next"
        Ups.save()
        MarketScan()
        Ups.status = "Finished"
        Ups.finish = datetime.datetime.now()
        Ups.save()
    except:
        pass
    finally:
        update_web()
