package main

import (
	//"database/sql"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"
)

/* ----------------------------------------------------------------------------
                           START OF STRUCTS
-----------------------------------------------------------------------------*/

type Slot struct {
	name      string
	hero      string
	items     []string
	baseitems []*BaseItem
}

func (self *Slot) Null() {
	self.name = ""
	self.hero = ""
	self.items = nil
	self.baseitems = nil
}

func (self *Slot) SlotLink() string {
	if self.Hero() != nil {
		return self.HeroLink() + "/" + self.Name()
	}
	return "/dota/player/" + self.Name()
}

func (self *Slot) HeroLink() string {
	if self.Hero() != nil {
		return self.Hero().HeroLink()
	}
	return ""
}

func (self *Slot) Hero() *Hero {
	for _, hero := range HeroCache {
		if strings.ToLower(hero.Name) == strings.ToLower(self.hero) {
			return hero
		}
	}
	return nil
}

func (self *Slot) Name() string {
	return strings.Title(self.name)
}

func (self *Slot) Items() []*BaseItem {
	if len(self.baseitems) > 0 {
		return self.baseitems
	}
	var items []*BaseItem
	for _, item := range self.items {
		i := BaseItemCache[strings.ToLower(item)]
		if i.OnMarket() {
			items = append(items, i)
		}
	}
	self.baseitems = items
	return self.baseitems
}

func (self *Slot) Image() string {
	image := ""
	for _, i := range self.Items() {
		image = i.Image()
	}
	return image
}

type Hero struct {
	Name        string
	Heroid      string
	Image       string
	items       []*BaseItem
	bundles     []*BaseItem
	slots       []*Slot
	sets        []*Set
	marketitems []*Item
}

func (self *Hero) Null() {
	self.Name = ""
	self.Heroid = ""
	self.Image = ""
	self.items = nil
	self.bundles = nil
	self.slots = nil
	self.sets = nil
	self.marketitems = nil
}

func (self *Hero) HeroLink() string {
	return "/dota/hero/" + self.Name
}

func (self *Hero) MarketItems() []*Item {
	if len(self.marketitems) > 0 {
		return self.marketitems
	}
	var items []*Item
	for _, base := range self.Items() {
		for _, market := range base.MarketItems() {
			items = append(items, market)
		}
	}
	self.marketitems = items
	return self.marketitems
}

func (self *Hero) Items() []*BaseItem {
	if len(self.items) > 0 {
		return self.items
	}
	var items []*BaseItem
	for _, base := range BaseItemCache {
		if base.Heroid == self.Heroid {
			items = append(items, base)
		}
	}
	self.items = items
	return self.items
}

func (self *Hero) Sets() []*Set {
	if len(self.sets) > 0 {
		return self.sets
	}
	sets := []*Set{}
	for _, item := range self.Items() {
		if item.Set() != nil {
			add := true
			for _, set := range sets {
				if set.Name == item.Set().Name {
					add = false
					break
				}
			}
			if add == true && item.Set() != nil && item.Set().Name != "" {
				sets = append(sets, item.Set())
			}
		}
	}
	self.sets = sets
	return self.sets
}

func (self *Hero) Bundles() []*BaseItem {
	if len(self.bundles) > 0 {
		return self.bundles
	}
	bundles := []*BaseItem{}
	for _, base := range self.Items() {
		if base.slotname == "bundle" && base.OnMarket() == true {
			add := true
			for _, b := range bundles {
				if b.Name == base.Name || len(base.Image()) == 0 {
					add = false
					break
				}
			}
			if add == true {
				bundles = append(bundles, base)
			}
		}
	}
	self.bundles = bundles
	return self.bundles
}

func (self *Hero) Slots() []*Slot {
	if len(self.slots) > 0 {
		return self.slots
	}
	slots := []*Slot{}
	for _, base := range self.Items() {
		if base.slotname != "" && base.slotname != "none" && base.slotname != "bundle" && base.OnMarket() == true {
			add := true
			for _, b := range slots {
				if b.name == base.Slot().name && len(b.Image()) > 0 {
					add = false
					break
				}
			}
			if add == true {
				slots = append(slots, base.Slot())
			}
		}
	}
	self.slots = slots
	return self.slots
}

func (self *Hero) Slot(slot string) *Slot {
	for _, base := range self.Items() {
		if strings.ToLower(base.slotname) == strings.ToLower(slot) && base.OnMarket() == true {
			return base.Slot()
		}
	}
	return nil
}

type LootList struct {
	name      string
	items     []string
	baseitems []*BaseItem
	box       *BaseItem
}

func (self *LootList) Null() {
	self.name = ""
	self.items = nil
	self.baseitems = nil
	self.box = nil
}

func (self *LootList) Box() *BaseItem {
	if self.box != nil {
		return self.box
	}
	for _, item := range BaseItemCache {
		if item.lootlist == self.name {
			self.box = item
			return self.box
		}
	}
	return nil
}

func (self *LootList) Items() []*BaseItem {
	if len(self.baseitems) > 0 {
		return self.baseitems
	}
	bases := []*BaseItem{}
	for _, i := range self.items {
		if item, ok := BaseItemCache[strings.ToLower(i)]; ok {
			bases = append(bases, item)
		}
	}
	self.baseitems = bases
	return self.baseitems
}

type MiscBucket struct {
	name  string
	items []*BaseItem
}

func (self *MiscBucket) Null() {
	self.name = ""
	self.items = nil
}

func (self *MiscBucket) Items() []*BaseItem {
	sort.Sort(baseName(self.items))
	return self.items
}

func (self *MiscBucket) Name() string {
	return strings.Title(self.name)
}

func (self *MiscBucket) Image() string {
	for _, item := range self.Items() {
		if item.Image() != "" {
			return item.Image()
		}
	}
	return ""
}

type ItemBucket struct {
	Name     string
	Low      string
	High     string
	Image    string
	Rarity   string
	Items    map[string]*Item
	Currency string
}

func (self ItemBucket) Null() {
	self.Name = ""
	self.Low = ""
	self.High = ""
	self.Image = ""
	self.Rarity = ""
	self.Items = nil
	self.Currency = ""
}

func (self ItemBucket) Price() string {
	hp := ""
	lp := ""
	if self.Currency != "" {
		lp, hp = self.ConvertPrice(self.Currency)
	}
	return lp + hp
}

func (self *ItemBucket) ConvertPrice(curid string) (string, string) {
	if cur, ok := CurCache[strings.ToLower(curid)]; ok {
		l, _ := strconv.ParseFloat(self.Low, 64)
		h, _ := strconv.ParseFloat(self.High, 64)
		y, _ := strconv.ParseFloat(cur.Rate, 64)
		low := fmt.Sprintf("%.2f", (l * y))
		low = strings.Replace(low, ".", cur.Separator, -1)
		high := fmt.Sprintf("%.2f", (h * y))
		high = strings.Replace(high, ".", cur.Separator, -1)
		return low, high
	}
	return "", ""
}

type BaseItem struct {
	Name         string
	Description  string
	image        string
	slotname     string
	set          string
	Type         string
	creationdate string
	Heroid       string
	Model        string
	Particle     string
	lootlist     string
	StorePrice   string
	rarity       string
	marketitems  []*Item
	list         *LootList
	hero         *Hero
	slot         *Slot
}

func (self *BaseItem) Null() {
	self.Name = ""
	self.Description = ""
	self.image = ""
	self.slotname = ""
	self.set = ""
	self.Type = ""
	self.creationdate = ""
	self.Heroid = ""
	self.Model = ""
	self.Particle = ""
	self.lootlist = ""
	self.StorePrice = ""
	self.rarity = ""
	self.marketitems = nil
	self.list = nil
	self.hero = nil
	self.slot = nil
}

func (self *BaseItem) ItemLink() string {
	if self.Hero() != nil {
		return self.SlotLink() + "/" + self.Name
	}
	return "/dota/item/" + self.Name
}

func (self *BaseItem) SetLink() string {
	if self.Set() != nil {
		return self.Set().SetLink()
	}
	return ""
}

func (self *BaseItem) HeroLink() string {
	if self.Hero() != nil {
		return self.Hero().HeroLink()
	}
	return ""
}

func (self *BaseItem) SlotLink() string {
	if self.Slot() != nil {
		return self.Slot().SlotLink()
	}
	return "/dota/player/" + self.slotname
}

func (self *BaseItem) LootList() *LootList {
	if self.list != nil {
		return self.list
	}
	if list, ok := LootCache[strings.ToLower(self.lootlist)]; ok {
		self.list = list
		return self.list
	}
	for _, list := range LootCache {
		for _, item := range list.Items() {
			if item.Name == self.Name {
				self.list = list
				return self.list
			}
		}
	}
	return nil
}

func (self *BaseItem) Box() *BaseItem {
	if self.LootList() != nil && self.LootList().Box() != nil {
		return self.LootList().Box()
	}
	return nil
}

func (self *BaseItem) CreationDate() time.Time {
	t, _ := time.Parse("2006-01-02", self.creationdate)
	return t
}

func (self *BaseItem) Date() string {
	return self.CreationDate().Format("Jan 2, 2006")
}

func (self *BaseItem) Loot() *LootList {
	return LootCache[self.lootlist]
}

func (self *BaseItem) Image() string {
	for _, item := range self.MarketItems() {
		if item.Image() != "" {
			return item.Image()
		}
	}
	return self.image
}

func (self *BaseItem) Price() []string {
	low := "1000.00"
	high := "0.00"
	for _, item := range self.MarketItems() {
		p, _ := strconv.ParseFloat(item.price, 64)
		l, _ := strconv.ParseFloat(low, 64)
		h, _ := strconv.ParseFloat(high, 64)
		if p < l {
			low = item.price
		}
		if p > h {
			high = item.price
		}
	}
	if high == low {
		return SinglePrice(high)
	}
	if high != "0.00" && low != "1000.00" {
		return DoublePrice(low, high)
	}
	return []string{}
}

func (self *BaseItem) Rarity() string {
	if self.rarity != "" {
		return self.rarity
	}
	for _, item := range self.MarketItems() {
		if item.Rarity != "" {
			return item.Rarity
		}
	}
	return ""
}

func (self *BaseItem) MarketItems() []*Item {
	if len(self.marketitems) > 0 {
		return self.marketitems
	}
	var items []*Item
	for _, item := range ItemCache[strings.ToLower(self.Name)] {
		items = append(items, item)
	}
	self.marketitems = items
	return self.marketitems
}

func (self *BaseItem) Set() *Set {
	if set, ok := SetCache[self.set]; ok {
		return set
	}
	return nil
}

func (self *BaseItem) SetName() string {
	if self.Set() != nil {
		return self.Set().Name
	}
	return ""
}

func (self *BaseItem) HeroName() string {
	if self.Hero() != nil {
		return self.Hero().Name
	}
	return ""
}

func (self *BaseItem) SlotName() string {
	if self.Slot() != nil {
		return self.Slot().Name()
	}
	return strings.Title(self.slotname)
}

func (self *BaseItem) Slot() *Slot {
	if self.slot != nil {
		return self.slot
	}
	if self.Hero() != nil {
		if slot, ok := SlotCache[strings.ToLower(self.Hero().Name)][strings.ToLower(self.slotname)]; ok {
			self.slot = slot
			return self.slot
		}
	}
	return nil
}

func (self *BaseItem) OnMarket() bool {
	if _, ok := ItemCache[strings.ToLower(self.Name)]; ok {
		return true
	}
	return false
}

func (self *BaseItem) Hero() *Hero {
	if hero, ok := HeroCache[self.Heroid]; ok {
		return hero
	}
	return nil
}

type Item struct {
	Name     string
	Game     string
	price    string
	Link     string
	Rarity   string
	base     string
	Quality  string
	Currency string
	Count    string
	image    string
	baseitem *BaseItem
	hero     *Hero
	slot     *Slot
	set      *Set
}

func (self *Item) Null() {
	self.Name = ""
	self.Game = ""
	self.price = ""
	self.Link = ""
	self.Rarity = ""
	self.base = ""
	self.Quality = ""
	self.Currency = ""
	self.Count = ""
	self.image = ""
	self.baseitem = nil
	self.hero = nil
	self.slot = nil
	self.set = nil
}

func (self *Item) Set() *Set {
	if self.set != nil {
		return self.set
	}
	if self.Base() == nil {
		return nil
	}
	self.set = self.Base().Set()
	return self.set
}

func (self *Item) Slot() *Slot {
	if self.slot != nil {
		return self.slot
	}
	if self.Base() == nil {
		return nil
	}
	self.slot = self.Base().Slot()
	return self.slot
}

func (self *Item) Hero() *Hero {
	if self.hero != nil {
		return self.hero
	}
	if self.Base() == nil {
		return nil
	}
	self.hero = self.Base().Hero()
	return self.hero
}

func (self *Item) ItemLink() string {
	if self.Base() != nil {
		return self.Base().ItemLink()
	}
	return ""
}

func (self *Item) SetName() string {
	if self.Base() != nil {
		return self.Base().SetName()
	}
	return ""
}

func (self *Item) SetLink() string {
	if self.Base() != nil {
		return self.Base().SetLink()
	}
	return ""
}

func (self *Item) HeroName() string {
	if self.Base() != nil {
		return self.Base().HeroName()
	}
	return ""
}

func (self *Item) HeroLink() string {
	if self.Base() != nil {
		return self.Base().HeroLink()
	}
	return ""
}

func (self *Item) SlotName() string {
	if self.Base() != nil {
		return self.Base().SlotName()
	}
	return ""
}

func (self *Item) SlotLink() string {
	if self.Base() != nil {
		return self.Base().SlotLink()
	}
	return ""
}

func (self *Item) Base() *BaseItem {
	if self.baseitem != nil {
		return self.baseitem
	}
	if base, ok := BaseItemCache[strings.ToLower(self.base)]; ok {
		self.baseitem = base
		return self.baseitem
	}
	return nil
}

func (self *Item) Image() string {
	if self.image != "" {
		return self.image + imgWidth + "x" + imgHeight
	}
	return ""
}

func (self Item) Price() []string {
	return SinglePrice(self.price)
}

func (self *Item) ConvertPrice(curid string) string {
	if cur, ok := CurCache[strings.ToLower(curid)]; ok {
		x, _ := strconv.ParseFloat(self.price, 64)
		y, _ := strconv.ParseFloat(cur.Rate, 64)
		p := fmt.Sprintf("%.2f", (x * y))
		p = strings.Replace(p, ".", cur.Separator, -1)
		return p
	}
	return ""
}

type Set struct {
	Id          string
	Name        string
	Image       string
	StoreBundle string
	items       []string
	baseitems   []*BaseItem
}

func (self *Set) Null() {
	self.Id = ""
	self.Name = ""
	self.Image = ""
	self.StoreBundle = ""
	self.items = nil
	self.baseitems = nil
}

func (self *Set) Price() []string {
	total_low := 0.00
	total_high := 0.00
	for _, item := range self.Items() {
		ilow := "1000.00"
		ihigh := "0.00"
		// TODO make set Price() work
		for _, i := range item.MarketItems() {
			p, _ := strconv.ParseFloat(i.price, 64)
			l, _ := strconv.ParseFloat(ilow, 64)
			h, _ := strconv.ParseFloat(ihigh, 64)
			if p < l {
				ilow = i.price
			}
			if p > h {
				ihigh = i.price
			}
		}
		il, _ := strconv.ParseFloat(ilow, 64)
		ih, _ := strconv.ParseFloat(ihigh, 64)
		total_low = total_low + il
		total_high = total_high + ih
	}
	low := strconv.FormatFloat(total_low, 'f', 2, 64)
	high := strconv.FormatFloat(total_high, 'f', 2, 64)
	if high == low {
		return SinglePrice(high)
	}
	if high != "0.00" && low != "1000.00" {
		return DoublePrice(low, high)
	}
	return []string{}
}

func (self *Set) HeroName() string {
	if self.Hero() != nil {
		return self.Hero().Name
	}
	return ""
}

func (self *Set) HeroLink() string {
	if self.Hero() != nil {
		return self.Hero().HeroLink()
	}
	return ""
}

func (self *Set) SetLink() string {
	return "/dota/set/" + self.Name
}

func (self *Set) Items() []*BaseItem {
	if len(self.baseitems) > 0 {
		return self.baseitems
	}
	var items []*BaseItem
	for _, item := range self.items {
		i := BaseItemCache[strings.ToLower(item)]
		if i.OnMarket() {
			items = append(items, i)
		}
	}
	self.baseitems = items
	return self.baseitems
}

func (self *Set) Hero() *Hero {
	for _, item := range self.Items() {
		if item.Hero() != nil {
			return item.Hero()
		}
	}
	return nil
}

type Err struct {
	E        error
	Time     time.Time
	Severity string
	Source   string
}

func (e Err) Save() {
	qry := "insert into error(source, error, time, severity) values ($1, $2, $3, $4);"
	_ = db.QueryRow(qry, e.Source, e.E, e.Time, e.Severity)
}

type bName []*ItemBucket

func (a bName) Len() int           { return len(a) }
func (a bName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a bName) Less(i, j int) bool { return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name) }

type iRare []*Item

type iPrice []*Item

func (a iPrice) Len() int           { return len(a) }
func (a iPrice) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a iPrice) Less(i, j int) bool { return strings.ToLower(a[i].price) < strings.ToLower(a[j].price) }

type iName []*Item

func (a iName) Len() int           { return len(a) }
func (a iName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a iName) Less(i, j int) bool { return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name) }

type baseName []*BaseItem

func (a baseName) Len() int           { return len(a) }
func (a baseName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a baseName) Less(i, j int) bool { return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name) }

type ByPrice []*Item

func (a ByPrice) Len() int      { return len(a) }
func (a ByPrice) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByPrice) Less(i, j int) bool {
	y, _ := strconv.ParseFloat(string(a[i].price), 64)
	z, _ := strconv.ParseFloat(string(a[j].price), 64)
	return y < z
}

type ByName []*Hero

func (a ByName) Len() int           { return len(a) }
func (a ByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByName) Less(i, j int) bool { return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name) }

type MiscsName []*MiscBucket

func (a MiscsName) Len() int      { return len(a) }
func (a MiscsName) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a MiscsName) Less(i, j int) bool {
	return strings.ToLower(a[i].Name()) < strings.ToLower(a[j].Name())
}
