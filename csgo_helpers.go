package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func ParseContainerContents(gun string) *GunBucket {
	if strings.Contains(gun, "|") {
		// it's /probably/ a gun with a skin, split em up
		split := strings.Split(gun, "|")
		base := strings.Trim(split[0], " ")
		skin := strings.Trim(split[1], " ")
		bucket := GetGunBucket(base, skin)
		if len(bucket.Guns) > 0 {
			return bucket
		}
	}
	if strings.Contains(gun, "(") {
		// item has a (), likely a sticker with (Holo) or (Foil)
		gun = strings.Replace(gun, ")", "", -1)
		split := strings.Split(gun, "(")
		base := strings.Trim(split[0], " ")
		exterior := strings.Trim(split[1], " ")
		if exterior == "H" {
			exterior = "Holo"
		}
		if AmIASticker(base) {
			return GetGunBucket("Sticker", base)
		}
	}
	if AmIASticker(gun) {
		return GetGunBucket("Sticker", gun)
	}
	b := strings.Split(gun, "|")
	base := strings.Trim(b[0], " ")
	if AmIASticker(base) {
		return GetGunBucket("Sticker", base)
	}
	fmt.Println("No idea what this is:", gun)
	return nil
}

func badBase(gun *CSItem) bool {
	if gun.Type == "Sticker" {
		return true
	}
	if gun.Type == "Container" {
		return true
	}
	if gun.Type == "Key" {
		return true
	}
	if gun.Type == "Pass" {
		return true
	}
	return false
}

func CSGOFilter(t, b, q, e, stat, souv, lowprice, highprice string, cur *Currency) []*CSItem {
	fmt.Println("Searching for types", t, "and base", b, "and exterior", e, "between", lowprice, "and", highprice)
	a := []*CSItem{}
	for _, guns := range GunCache {
		for _, gun := range guns {
			l, _ := strconv.ParseFloat(string(lowprice), 64)
			h, _ := strconv.ParseFloat(string(highprice), 64)
			if csgoMatch(gun, t, b, q, e, stat, souv, l, h) == true {
				g := gun
				addme := true
				for _, gat := range a {
					if gat == g {
						addme = false
					}
				}
				if addme == true {
					a = append(a, g)
				}
			}
		}
	}
	if len(a) > 0 {
		return a
	}
	return nil
}

func csgoMatch(gun *CSItem, types, bases, qualities, exteriors, stattrak, souvenir string, lowprice, highprice float64) bool {
	var validType = false
	if types != "All" {
		for _, t := range strings.Split(types, ",") {
			if strings.ToLower(t) == strings.ToLower(gun.Type) {
				validType = true
			}
		}
	}
	var validBase = false
	if bases != "All" {
		if strings.Contains(strings.ToLower(bases), strings.ToLower(gun.Base)) {
			validBase = true
		}
	}
	if types != "All" || bases != "All" {
		if validBase == false && validType == false {
			return false
		}
	}
	if qualities != "All" {
		if !strings.Contains(strings.ToLower(qualities), strings.ToLower(gun.Quality)) {
			return false
		}
		if gun.Quality == "" {
			return false
		}
	}
	if exteriors != "All" {
		if !strings.Contains(strings.ToLower(exteriors), strings.ToLower(gun.Exterior)) {
			return false
		}
		if gun.Exterior == "" {
			return false
		}
	}
	p, _ := strconv.ParseFloat(string(gun.price), 64)
	if p < lowprice || p > highprice {
		return false
	}
	if stattrak == "true" {
		if gun.Stattrak == false {
			return false
		}
	}
	if souvenir == "true" {
		if gun.Souvenir == false {
			return false
		}
	}
	return true
}

func GetCollectionBuckets(collection string) []*GunBucket {
	a := []*GunBucket{}
	matches := make(map[string]map[string]bool)
	for _, guns := range GunCache {
		for _, gun := range guns {
			if gun.Collection == collection && gun.Type != "Container" {
				if _, ok := matches[gun.Base]; ok {
					matches[gun.Base][gun.Skin] = true
				} else {
					matches[gun.Base] = make(map[string]bool)
					matches[gun.Base][gun.Skin] = true
				}
			}
		}
	}
	for base, skins := range matches {
		for skin, _ := range skins {
			a = append(a, GetGunBucket(base, skin))
		}
	}
	sort.Sort(GBByQuality(a))
	if len(a) > 0 {
		return a
	}
	return nil
}

func GetQualityBuckets(quality string) []*GunBucket {
	a := []*GunBucket{}
	matches := make(map[string]map[string]bool)
	for _, guns := range GunCache {
		for _, gun := range guns {
			if gun.Quality == quality {
				if _, ok := matches[gun.Base]; ok {
					matches[gun.Base][gun.Skin] = true
				} else {
					matches[gun.Base] = make(map[string]bool)
					matches[gun.Base][gun.Skin] = true
				}
			}
		}
	}
	for base, skins := range matches {
		for skin, _ := range skins {
			a = append(a, GetGunBucket(base, skin))
		}
	}
	sort.Sort(GBByName(a))
	if len(a) > 0 {
		return a
	}
	return nil
}

func GetSkinBuckets(skin string) []*GunBucket {
	a := []*GunBucket{}
	for base, _ := range Skins[strings.ToLower(skin)] {
		a = append(a, GetGunBucket(base, skin))
	}
	sort.Sort(GBByName(a))
	if len(a) > 0 {
		return a
	}
	return nil
}

func GetGunBucket(gun string, skin string) *GunBucket {
	bucket := &GunBucket{}
	bucket.Base = gun
	if skin != "" {
		bucket.Skin = skin
	}
	bucket.Guns = []*CSItem{}
	bucket.Low = "1000.00" // I have to parse these prices
	bucket.High = "0.00"

	for _, gun := range GunCache[strings.ToLower(gun)] {
		if strings.ToLower(gun.Skin) == strings.ToLower(skin) {
			bucket.Skin = gun.Skin
			bucket.Base = gun.Base
			bucket.Description = gun.Description
			if gun.Stattrak == true {
				bucket.Stattrak = true
			}
			if gun.Star == true {
				bucket.Star = true
			}
			if gun.Souvenir == true {
				bucket.Souvenir = true
			}
			if gun.Description != "" {
				bucket.Description = gun.Description
			}
			if gun.Quality != "" {
				bucket.Quality = gun.Quality
			}
			if gun.Type != "" {
				bucket.Type = gun.Type
			}
			if gun.Collection != "" {
				bucket.Collection = gun.Collection
			}
			l, _ := strconv.ParseFloat(string(bucket.Low), 64)
			h, _ := strconv.ParseFloat(string(bucket.High), 64)
			g, _ := strconv.ParseFloat(string(gun.price), 64)
			if g < l {
				bucket.Low = gun.price
			}
			if g > h {
				bucket.High = gun.price
			}
			bucket.Guns = append(bucket.Guns, gun)
		}
	}
	return bucket
}

func AmIASticker(name string) bool {
	for _, guns := range GunCache {
		for _, gun := range guns {
			if strings.ToLower(gun.Base) == "sticker" && strings.ToLower(gun.Skin) == strings.ToLower(name) {
				return true
			}
		}
	}
	return false
}
