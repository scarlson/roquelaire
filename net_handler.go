package main

import (
	"encoding/json"
	"fmt"
	"net"
	"strings"
)

func netListener() {
	if listening {
		return
	}
	listening = true
	l, e := net.Listen("tcp", listenAddr)
	if e != nil {
		// listener failed to open, a big deal, usually because it's already running
		LogError(e, "netListener()", "High")
		return
	}
	var s string
	for {
		c, _ := l.Accept()
		_, e := fmt.Fscan(c, &s)
		if e != nil {
			// we received bad data, not a big deal
			LogError(e, "netListener()", "Low")
		} else {
			s = strings.Replace(s, "*", " ", -1)
			fmt.Fprint(c, s)
			cmd := &Cmd{}
			e := json.Unmarshal([]byte(s), &cmd)
			if e != nil {
				// we received bad data, not a big deal
				LogError(e, "netListener()", "Low")
			}
			//fmt.Println("Cmd received:", cmd)
			if strings.ToLower(cmd.Type) == "price" {
				priceCmd(cmd.Body)
			}
			if strings.ToLower(cmd.Type) == "update" {
				fmt.Println("Received an update request!")
				CacheHeroes()
				CacheItems()
				CacheSets()
				CacheSlots()
				CachePlayerItems()
				CacheCurs()
				CacheGuns()
				CacheBoxes()
				CacheKeys()
				CacheSkins()
				CacheTypes()
				CacheBases()
			}
		}
	}
}

func priceCmd(s string) {
	price := &Price{}
	e := json.Unmarshal([]byte(s), &price)
	if e != nil {
		LogError(e, "priceCmd()", "Low")
	}
	//fmt.Println("Received:", price)
	if ValidPrice(price.Price) {
		UpdateItem(price)
		priceHub.Broadcast <- s
	}
}
