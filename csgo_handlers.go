package main

import (
	//"fmt"
	"github.com/hoisie/mustache"
	"github.com/hoisie/web"
	"sort"
	"strings"
)

func typeHandler(ctx *web.Context, t string) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	bases := GunBases[strings.ToLower(t)]
	if bases == nil {
		ctx.Redirect(302, "/csgo/types")
		return ""
	}
	gbcs := []*GunBucketContainer{}
	for _, base := range bases {
		gbcs = append(gbcs, &GunBucketContainer{Name: base, Buckets: []*GunBucket{}})
	}
	matches := make(map[string]map[string]bool)
	for _, guns := range GunCache {
		for _, gun := range guns {
			if strings.ToLower(gun.Type) == strings.ToLower(t) {
				if _, ok := matches[gun.Base]; ok {
					matches[gun.Base][gun.Skin] = true
				} else {
					matches[gun.Base] = make(map[string]bool)
					matches[gun.Base][gun.Skin] = true
				}
			}
		}
	}
	for base, skins := range matches {
		for skin, _ := range skins {
			for _, gbc := range gbcs {
				if strings.ToLower(gbc.Name) == strings.ToLower(base) {
					gbc.Buckets = append(gbc.Buckets, GetGunBucket(base, skin))
				}
			}
		}
	}
	for _, gbc := range gbcs {
		sort.Sort(GBByQuality(gbc.Buckets))
	}
	data["type"] = strings.Title(t)
	sort.Sort(GBCByName(gbcs))
	data["gbcs"] = gbcs
	multiples := false
	for _, gbc := range gbcs {
		if !gbc.OnlyOne() {
			multiples = true
			break
		}
	}
	data["multiples"] = multiples
	return mustache.RenderFileInLayout("templates/type.html", "templates/base.html", data)
}

func skinsHandler(ctx *web.Context) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	gbcs := []*GunBucketContainer{}
	for skin, _ := range Skins {
		gbcs = append(gbcs, &GunBucketContainer{Name: skin, Buckets: SkinCache[strings.ToLower(skin)]})
	}
	sort.Sort(GBCByName(gbcs))
	data["gbcs"] = gbcs
	return mustache.RenderFileInLayout("templates/type.html", "templates/base.html", data)
}

func qualitiesHandler(ctx *web.Context) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	gbcs := []*GunBucketContainer{}
	for name, _ := range Qualities {
		gbcs = append(gbcs, &GunBucketContainer{Name: name, Buckets: GetQualityBuckets(name)})
	}
	sort.Sort(GBCByQuality(gbcs))
	data["gbcs"] = gbcs
	return mustache.RenderFileInLayout("templates/qualities.html", "templates/base.html", data)
}

func qualityHandler(ctx *web.Context, quality string) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	coll := GetQualityBuckets(quality)
	if coll == nil {
		ctx.Redirect(302, "/csgo/types")
		return ""
	}
	data["guns"] = coll
	data["quality"] = strings.Title(quality)
	return mustache.RenderFileInLayout("templates/quality.html", "templates/base.html", data)
}

func collectionsHandler(ctx *web.Context) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	gbcs := []*GunBucketContainer{}
	for name, _ := range Collections {
		gbcs = append(gbcs, &GunBucketContainer{Name: name, Buckets: GetCollectionBuckets(name)})
	}
	sort.Sort(GBCByName(gbcs))
	data["gbcs"] = gbcs
	if len(gbcs) == 0 {
		ctx.Redirect(302, "/csgo")
		return ""
	}
	return mustache.RenderFileInLayout("templates/collections.html", "templates/base.html", data)
}

func collectionHandler(ctx *web.Context, collection string) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	coll := GetCollectionBuckets(collection)
	if coll == nil {
		ctx.Redirect(302, "/csgo/collections")
		return ""
	}
	data["guns"] = coll
	data["collection"] = strings.Title(collection)
	for _, bucket := range coll {
		if bucket.Containers() != nil {
			boxes := bucket.Containers()
			box := &CSItem{}
			for _, b := range boxes {
				box = b.Item()
			}
			k := bucket.Key()
			if k != nil {
				key := k.Item()
				data["key"] = key
			}
			data["container"] = box
		}
	}
	return mustache.RenderFileInLayout("templates/collection.html", "templates/base.html", data)
}

func gunHandler(ctx *web.Context, base string, skin string) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	b := GetGunBucket(base, skin)
	if len(b.Guns) == 0 {
		ctx.Redirect(302, "/csgo/gun/"+base)
		return ""
	}
	data["guns"] = b
	if len(b.Quality) > 0 && strings.ContainsAny(strings.ToLower(string(b.Quality[0])), "aeiou") {
		data["addn"] = "n"
	}
	if b.Type == "Container" {
		if _, ok := BoxCache[strings.ToLower(b.Base)]; ok {
			guns := []*GunBucket{}
			for _, bucket := range BoxCache[strings.ToLower(b.Base)].Guns {
				guns = append(guns, bucket)
			}
			sort.Sort(GBByQuality(guns))
			data["contents"] = guns
			box := BoxCache[strings.ToLower(b.Base)].Item()
			coll := BoxCache[strings.ToLower(b.Base)].Collection
			if coll != "" {
				data["collection"] = BoxCache[strings.ToLower(b.Base)].Collection
			}
			data["imabox"] = box
			k := BoxCache[strings.ToLower(b.Base)].Key()
			if k != nil {
				key := k.Item()
				data["boxkey"] = key
				data["HasKey"] = true
			}
		}
	}
	if b.Type == "Key" {
		boxes := KeyCache[strings.ToLower(b.Base)].Containers()
		if len(boxes) == 1 {
			data["OneKeyBox"] = true
			box := boxes[0].Item()
			data["keybox"] = box
			data["collection"] = boxes[0].Collection
		}
		key := KeyCache[strings.ToLower(b.Base)].Item()
		data["imakey"] = key
		boxen := []*CSItem{}
		for _, box := range boxes {
			b := box.Item()
			boxen = append(boxen, b)
		}
		data["boxes"] = boxen
	}
	if b.Collection != "" {
		data["collection"] = b.Collection
	}
	if len(b.Guns) == 1 {
		gun := b.Guns[0]
		data["gun"] = gun
	}
	return mustache.RenderFileInLayout("templates/gun.html", "templates/base.html", data)
}

func baseHandler(ctx *web.Context, base string) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	a := []*GunBucket{}
	skins := make(map[string]bool)
	Type := ""
	for _, base := range GunCache[strings.ToLower(base)] {
		skins[base.Skin] = true
	}
	for skin, _ := range skins {
		a = append(a, GetGunBucket(base, skin))
	}

	if len(a) == 0 {
		ctx.Redirect(302, "/csgo/types")
		return ""
	}

	for t, bases := range GunBases {
		for _, b := range bases {
			if b == base {
				Type = t
			}
		}
	}

	data["base"] = strings.Title(base)
	data["type"] = strings.Title(Type)
	sort.Sort(GBByQuality(a))
	data["guns"] = a
	if len(a) > 0 {
		data["Description"] = a[0].Description
	}
	return mustache.RenderFileInLayout("templates/gunbase.html", "templates/base.html", data)
}

func skinHandler(ctx *web.Context, skin string) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	data["guns"] = GetSkinBuckets(skin)
	if data["guns"] == nil {
		ctx.Redirect(302, "/csgo")
		return ""
	}
	data["skin"] = strings.Title(skin)
	return mustache.RenderFileInLayout("templates/skin.html", "templates/base.html", data)
}

func csgoGet(ctx *web.Context) string {
	data := make(map[string]interface{})
	types := []*Type{}
	for _, ty := range TypeCache {
		types = append(types, ty)
	}
	data["types"] = types
	return mustache.RenderFileInLayout("templates/csgo.html", "templates/base.html", data)
}

func typesHandler(ctx *web.Context) string {
	data := make(map[string]interface{})
	cur := getUserCurrency(ctx)
	data["cur"] = cur
	types := []*Type{}
	for _, ty := range TypeCache {
		types = append(types, ty)
	}
	sort.Sort(TypeByName(types))
	data["types"] = types
	return mustache.RenderFileInLayout("templates/types.html", "templates/base.html", data)
}

func searchHandler(ctx *web.Context) string {
	data := make(map[string]interface{})
	var cur = getUserCurrency(ctx)

	params := ctx.Params

	// souvenir & stattrak logic
	var stat = params["stat"]
	var souv = params["souv"]

	if souv == "true" {
		data["souvenir"] = true
	}

	if stat == "true" {
		data["stattrak"] = true
	}
	// beginning of exterior logic
	var e = params["e"]
	var ae = []string{}
	var pe = []string{}

	// filter out submitted exteriors that aren't actual exteriors, no e=bullshit
	if e != "All" {
		for _, z := range strings.Split(e, ",") {
			for y, _ := range Exteriors {
				if strings.ToLower(z) == strings.ToLower(y) {
					ae = append(ae, y)
				}
			}
		}
		e = strings.Join(ae, ",")
	}

	if len(ae) > 0 {
		sort.Strings(ae)
		data["ae"] = ae
	}

	for x, _ := range Exteriors {
		add := true
		if x == "Red" {
			add = false
		}
		if x == "Blue" {
			add = false
		}
		for _, ax := range ae {
			if ax == x {
				add = false
			}
		}
		if add == true {
			pe = append(pe, x)
		}
	}
	sort.Strings(pe)
	data["pe"] = pe
	// end of exterior logic

	// beginning of base logic
	var b = params["b"]
	var ab = []*Base{}
	var pb = []*Base{}

	// filter out submitted typees that aren't actual typees, no t=bullshit
	if b != "All" {
		var bases = []string{}
		for _, z := range strings.Split(b, ",") {
			for _, y := range BaseCache {
				if strings.ToLower(z) == strings.ToLower(y.Name) {
					ab = append(ab, y)
					bases = append(bases, z)
				}
			}
		}
		b = strings.Join(bases, ",")
	}
	if len(ab) > 0 {
		sort.Sort(BaseByName(ab))
		data["ab"] = ab
	}

	for _, b := range BaseCache {
		add := true
		for _, a := range ab {
			if a.Name == b.Name {
				add = false
			}
		}
		if add == true {
			for _, t := range AcceptableTypes {
				if t == b.Type {
					pb = append(pb, b)
				}
			}
		}
	}
	sort.Sort(BaseByName(pb))
	data["pb"] = pb
	// end of base logic

	// beginning of quality logic
	var q = params["q"]

	if q != "All" {
		var rq = []string{}
		for _, z := range strings.Split(q, ",") {
			for y, _ := range Qualities {
				if strings.ToLower(z) == strings.ToLower(y) {
					rq = append(rq, z)
				}
			}
		}
		q = strings.Join(rq, ",")
	}

	// filter active/passive qualities
	aq := strings.Split(q, ",")
	pq := []string{}
	for qu, _ := range Qualities {
		found := false
		for _, qa := range aq {
			if strings.ToLower(qa) == strings.ToLower(qu) {
				found = true
			}
		}
		if !found {
			pq = append(pq, qu)
		}
	}
	if len(aq) > 0 && aq[0] != "" && aq[0] != "All" {
		sort.Strings(aq)
		data["aq"] = aq
	}
	sort.Strings(pq)
	data["pq"] = pq
	// end of quality logic

	// beginning of type logic
	var t = params["t"]
	var at = []*Type{}
	var pt = []*Type{}

	// filter out submitted typees that aren't actual typees, no t=bullshit
	if t != "All" {
		var types = []string{}
		for _, z := range strings.Split(t, ",") {
			for _, y := range TypeCache {
				if strings.ToLower(z) == strings.ToLower(y.Name) {
					at = append(at, y)
					types = append(types, z)
				}
			}
		}
		t = strings.Join(types, ",")
	}
	if len(at) > 0 {
		sort.Sort(TypeByName(at))
		data["t"] = at
	}

	for _, t := range TypeCache {
		add := true
		for _, a := range at {
			if a.Name == t.Name {
				add = false
			}
		}
		if add == true {
			pt = append(pt, t)
		}
	}
	sort.Sort(TypeByName(pt))
	data["tc"] = pt
	// end of type logic

	// beginning of price logic
	lp := params["lp"]
	hp := params["hp"]

	// convert these to USD if alt currency
	// if euro, lp = lp / euro rate
	// hp = hp / euro rate
	if cur != nil {
		lp = ReversePrice(lp, cur.Code)
		hp = ReversePrice(hp, cur.Code)
	}

	if lp == "" || !ValidPrice(lp) || lp == " 0.00" || lp == "0.00" {
		lp = MINPRICE
	}
	if hp == "" || !ValidPrice(hp) || hp == " 0.00" || hp == "0.00" {
		hp = MAXPRICE
	}

	data["minprice"] = MINPRICE
	data["maxprice"] = MAXPRICE
	if cur != nil {
		data["minprice"] = ConvertPrice(MINPRICE, cur.Code)
		data["maxprice"] = ConvertPrice(MAXPRICE, cur.Code)
		data["cur"] = cur
	}
	data["lp"] = lp
	data["hp"] = hp
	// end of price logic

	// one little line gets all the guns after validating parameters
	guns := CSGOFilter(t, b, q, e, stat, souv, lp, hp, cur)

	if len(guns) > 0 {
		data["derp"] = true
		data["guns"] = guns
	}

	return mustache.RenderFileInLayout("templates/search.html", "templates/base.html", data)
}

func CSGO404Handler(ctx *web.Context, lost string) {
	ctx.Redirect(302, "/csgo")
}
