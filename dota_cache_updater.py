#!/usr/bin/env python
import socket
import json
import string

HOST = ''
PORT = 55792


def update_web():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    derp = json.dumps({"type": "update", "body": "dota"})
    derp = string.replace(derp, " ", "*")
    print "sending", derp
    s.send(derp + " ")
    s.close()


if __name__ == "__main__":
    update_web()
