package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func FilterBucket(heroes, rarities, qualities, lowprice, highprice string) []*Item {
	fmt.Println("Searching for items for", heroes, "of", rarities, "rarity and", qualities, "quality between the prices of", lowprice, "and", highprice)
	var items []*Item
	l, _ := strconv.ParseFloat(string(lowprice), 64)
	h, _ := strconv.ParseFloat(string(highprice), 64)
	for _, v := range ItemCache {
		for _, i := range v {
			if len(i.base) > 0 && len(i.price) > 0 {
				if filterMatch(i, heroes, rarities, qualities, l, h) {
					items = append(items, i)
				}
			}
		}
	}
	return items
}

func filterMatch(item *Item, heroes, rarities, qualities string, lowprice, highprice float64) bool {
	if item.Base() == nil {
		return false
	}
	if heroes != "All" {
		if item.Base().Hero() == nil {
			return false
		}
		if !strings.Contains(strings.ToLower(heroes), strings.ToLower(item.Base().Hero().Name)) {
			return false
		}
		if item.Base().Hero().Name == "" {
			return false
		}
	}
	if rarities != "All" {
		// this returns commons when searching for uncommon.
		r := false
		for _, ra := range strings.Split(strings.ToLower(rarities), ",") {
			if ra == strings.ToLower(item.Rarity) {
				r = true
			}
		}
		if !r {
			return false
		}
		if len(item.Rarity) == 0 {
			return false
		}
	}
	if qualities != "All" {
		if !strings.Contains(strings.ToLower(qualities), strings.ToLower(item.Quality)) {
			return false
		}
	}
	p, _ := strconv.ParseFloat(string(item.price), 64)
	if p < lowprice || p > highprice {
		return false
	}
	if strings.Contains(strings.ToLower(item.SlotName()), strings.ToLower("retired")) {
		return false
	}
	return true
}

func getHero(name string) *Hero {
	for _, hero := range HeroCache {
		if strings.ToLower(hero.Name) == strings.ToLower(name) {
			return hero
		}
	}
	return nil
}

func getHeroes() []*Hero {
	heroes := []*Hero{}
	for _, h := range HeroCache {
		heroes = append(heroes, h)
	}
	sort.Sort(ByName(heroes))
	return heroes
}

func GetBucket(name string) *ItemBucket {
	bucket := &ItemBucket{}
	bucket.Name = name
	bucket.Low = "1000.00" // I have to parse these prices
	bucket.High = "0.00"
	bucket.Items = ItemCache[strings.ToLower(name)]
	for _, v := range bucket.Items {
		//bucket.Image = v.Image
		if len(v.Rarity) > 0 {
			bucket.Rarity = v.Rarity
		}
		l, _ := strconv.ParseFloat(string(bucket.Low), 64)
		h, _ := strconv.ParseFloat(string(bucket.High), 64)
		i, _ := strconv.ParseFloat(string(v.price), 64)
		if i < l {
			bucket.Low = v.price
		}
		if i > h {
			bucket.High = v.price
		}
	}
	if bucket.Low == "1000.00" {
		bucket.Low = "0"
	}
	return bucket
}

func UpdateItem(p *Price) {
	//find the base, get ItemCache[base], find the item, update price
	base := getItemBase(p.Name)
	for _, i := range ItemCache[strings.ToLower(base)] {
		if i.Name == p.Name {
			fmt.Println(p.Name, ":", i.Price, "->", p.Price)
			i.price = p.Price
			return
		}
	}
}

func getItemBase(name string) string {
	if strings.ToLower(name) == "inscribed gem" {
		return name
	}
	n := strings.Split(name, " ")
	// for q in quality, if name[0] == q: return name[1:] else return name
	for _, q := range QUALITIES {
		if strings.ToLower(q) == strings.ToLower(n[0]) {
			return strings.Join(n[1:], " ")
		}
	}
	return name
}
