function setCur(cur) {
    document.cookie = "cur=" + cur + ";path=/";
    document.location = document.location;
}
function setAUD(){setCur("AUD");}
$("#curmenu").append('<li><a id="AUD" href="">AUD</a></li>');
$("#AUD")[0].addEventListener("click", setAUD, false);
function setBRL(){setCur("BRL");}
$("#curmenu").append('<li><a id="BRL" href="">BRL</a></li>');
$("#BRL")[0].addEventListener("click", setBRL, false);
function setCAD(){setCur("CAD");}
$("#curmenu").append('<li><a id="CAD" href="">CAD</a></li>');
$("#CAD")[0].addEventListener("click", setCAD, false);
function setEUR(){setCur("EUR");}
$("#curmenu").append('<li><a id="EUR" href="">EUR</a></li>');
$("#EUR")[0].addEventListener("click", setEUR, false);
function setGBP(){setCur("GBP");}
$("#curmenu").append('<li><a id="GBP" href="">GBP</a></li>');
$("#GBP")[0].addEventListener("click", setGBP, false);
function setIDR(){setCur("IDR");}
$("#curmenu").append('<li><a id="IDR" href="">IDR</a></li>');
$("#IDR")[0].addEventListener("click", setIDR, false);
function setJPY(){setCur("JPY");}
$("#curmenu").append('<li><a id="JPY" href="">JPY</a></li>');
$("#JPY")[0].addEventListener("click", setJPY, false);
function setKRW(){setCur("KRW");}
$("#curmenu").append('<li><a id="KRW" href="">KRW</a></li>');
$("#KRW")[0].addEventListener("click", setKRW, false);
function setMXN(){setCur("MXN");}
$("#curmenu").append('<li><a id="MXN" href="">MXN</a></li>');
$("#MXN")[0].addEventListener("click", setMXN, false);
function setMYR(){setCur("MYR");}
$("#curmenu").append('<li><a id="MYR" href="">MYR</a></li>');
$("#MYR")[0].addEventListener("click", setMYR, false);
function setNOK(){setCur("NOK");}
$("#curmenu").append('<li><a id="NOK" href="">NOK</a></li>');
$("#NOK")[0].addEventListener("click", setNOK, false);
function setNZD(){setCur("NZD");}
$("#curmenu").append('<li><a id="NZD" href="">NZD</a></li>');
$("#NZD")[0].addEventListener("click", setNZD, false);
function setPHP(){setCur("PHP");}
$("#curmenu").append('<li><a id="PHP" href="">PHP</a></li>');
$("#PHP")[0].addEventListener("click", setPHP, false);
function setRUB(){setCur("RUB");}
$("#curmenu").append('<li><a id="RUB" href="">RUB</a></li>');
$("#RUB")[0].addEventListener("click", setRUB, false);
function setSGD(){setCur("SGD");}
$("#curmenu").append('<li><a id="SGD" href="">SGD</a></li>');
$("#SGD")[0].addEventListener("click", setSGD, false);
function setTHB(){setCur("THB");}
$("#curmenu").append('<li><a id="THB" href="">THB</a></li>');
$("#THB")[0].addEventListener("click", setTHB, false);
function setTRY(){setCur("TRY");}
$("#curmenu").append('<li><a id="TRY" href="">TRY</a></li>');
$("#TRY")[0].addEventListener("click", setTRY, false);
function setUAH(){setCur("UAH");}
$("#curmenu").append('<li><a id="UAH" href="">UAH</a></li>');
$("#UAH")[0].addEventListener("click", setUAH, false);
function setUSD(){setCur("USD");}
$("#curmenu").append('<li><a id="USD" href="">USD</a></li>');
$("#USD")[0].addEventListener("click", setUSD, false);
function setVND(){setCur("VND");}
$("#curmenu").append('<li><a id="VND" href="">VND</a></li>');
$("#VND")[0].addEventListener("click", setVND, false);
