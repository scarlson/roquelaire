function imgur() {
    img = document.getElementById("file").files[0];
    console.log("img:", img);
    upload(img);
}

function upload(file) {
    $.ajax({ 
        url: 'https://api.imgur.com/3/image',
        headers: {
            'Authorization': 'Client-ID 9681b4f90ca8a8f'
        },
        type: 'POST',
        data: {
            'image': file,
            'type': 'file',
        },
        success: function() { console.log('cool'); }
    });
}
