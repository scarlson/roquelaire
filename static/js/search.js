var type = ["All",];
var base = ["All",];
var qual = ["All",];
var exterior = ["All",];
var lp = ""
var hp = ""

var Types = $("#Types");
var typeModal = $("#typeModal .modal-body .row");
var Quals = $("#Qualities");
var qualModal = $("#qualModal .modal-body .row");
var Bases = $("#Bases");
var baseModal = $("#baseModal .modal-body .row");
var Exteriors = $("#Exteriors");
var exteriorModal = $("#exteriorModal .modal-body .row");
var all = '<div class="col-xs-4 col-sm-4 col-md-3 col-lg-2"><button disabled class="Btn" id="All">All</button></div>';

$("button.Type").click(function() {
    if (!$(this).hasClass("Active")) {
        typename = $(this).attr("id");
        $(this).addClass("Active");
        Types.prepend($(this).parent());
        $('#Types #All').parent().remove();
        $('#typeModal').modal('toggle');
        type.push(typename);
        if (type.indexOf("All") > -1) {
            type.splice(type.indexOf("All"), 1);
        }
    } else {
        typename = $(this).attr("id");
        $(this).removeClass("Active");
        typeModal.prepend($(this).parent());
        type.splice(type.indexOf(typename), 1);
        if (Types.children().length <= 1) {
            Types.prepend(all);
            type.push("All");
        }
    }
});

$("button.Base").click(function() {
    if (!$(this).hasClass("Active")) {
        basename = $(this).attr("id");
        $(this).addClass("Active");
        Bases.prepend($(this).parent());
        $('#Bases #All').parent().remove();
        $('#baseModal').modal('toggle');
        base.push(basename);
        if (base.indexOf("All") > -1) {
            base.splice(base.indexOf("All"), 1);
        }
    } else {
        basename = $(this).attr("id");
        $(this).removeClass("Active");
        baseModal.prepend($(this).parent());
        base.splice(base.indexOf(basename), 1);
        if (Bases.children().length <= 1) {
            Bases.prepend(all);
            base.push("All");
        }
    }
});

$("button.Exterior").click(function() {
    if (!$(this).hasClass("Active")) {
        ex = $(this).attr("id");
        $(this).addClass("Active");
        Exteriors.prepend($(this).parent());
        $('#Exteriors #All').parent().remove();
        $('#exteriorModal').modal('toggle');
        exterior.push(ex);
        if (exterior.indexOf("All") > -1) {
            exterior.splice(exterior.indexOf("All"), 1);
        }
    } else {
        ex = $(this).attr("id");
        $(this).removeClass("Active");
        exteriorModal.prepend($(this).parent());
        exterior.splice(exterior.indexOf(ex), 1);
        if (Exteriors.children().length <= 1) {
            Exteriors.prepend(all);
            exterior.push("All");
        }
    }
});

$("button.Quality").click(function() {
    if (!$(this).hasClass("Active")) {
        q = $(this).attr("id");
        $(this).addClass("Active");
        Quals.prepend($(this).parent());
        $('#Qualities #All').parent().remove();
        $('#qualModal').modal('toggle');
        qual.push(q);
        if (qual.indexOf("All") > -1) {
            qual.splice(qual.indexOf("All"), 1);
        }
    } else {
        q = $(this).attr("id");
        $(this).removeClass("Active");
        qualModal.prepend($(this).parent());
        qual.splice(qual.indexOf(q), 1);
        if (Quals.children().length <= 1) {
            Quals.prepend(all);
            qual.push("All");
        }
    }
});

$("button#Search").click(function() {
    search();
});

function search() {
    //console.log(hero, qual, rare)
    var lprice = $("#lprice").html();
    var hprice = $("#hprice").html();
    var souv = $("#souv").prop("checked");
    var stat = $("#stat").prop("checked");
    url = "http://" + location.host + "/csgo/search?t=" + encodeURIComponent(type) + "&b=" + encodeURIComponent(base) + "&e=" + encodeURIComponent(exterior) + "&q=" + encodeURIComponent(qual) + "&stat=" + encodeURIComponent(stat) + "&souv=" + encodeURIComponent(souv) + "&lp=" + encodeURIComponent(lprice) + "&hp=" + encodeURIComponent(hprice);
    location.href = url
};
