var hero = ["All",];
var qual = ["All",];
var rare = ["All",];
var lp = ""
var hp = ""

var Heroes = $("#Heroes");
var Qualities = $("#Qualities");
var Rarities = $("#Rarities");
var heroModal = $("#heroModal .modal-body .row");
var qualModal = $("#qualModal .modal-body .row");
var rareModal = $("#rareModal .modal-body .row");
var all = '<div class="col-xs-4 col-sm-4 col-md-3 col-lg-2"><button disabled class="Btn" id="All">All</button></div>';

$("button.Hero").click(function() {
    if (!$(this).hasClass("Active")) {
        heroname = $(this).attr("id");
        $(this).addClass("Active");
        Heroes.prepend($(this).parent());
        $('#Heroes #All').parent().remove();
        $('#heroModal').modal('toggle');
        hero.push(heroname);
        if (hero.indexOf("All") > -1) {
            hero.splice(hero.indexOf("All"), 1);
        }
    } else {
        heroname = $(this).attr("id");
        $(this).removeClass("Active");
        heroModal.prepend($(this).parent());
        hero.splice(hero.indexOf(heroname), 1);
        if (Heroes.children().length <= 1) {
            Heroes.prepend(all);
            hero.push("All");
        }
    }
    //console.log(hero);
});

$("button.Quality").click(function() {
    if (!$(this).hasClass("Active")) {
        $(this).addClass("Active");
        Qualities.prepend($(this).parent());
        $('#Qualities #All').parent().remove();
        $('#qualModal').modal('toggle');
        qual.push($(this).attr("id"));
        if (qual.indexOf("All") > -1) {
            qual.splice(qual.indexOf("All"), 1);
        }
    } else {
        $(this).removeClass("Active");
        qualModal.prepend($(this).parent());
        qual.splice(qual.indexOf($(this).attr("id")), 1);
        if (Qualities.children().length <= 1) {
            Qualities.prepend(all);
            qual.push("All");
        }
    }
    //console.log(qual);
});

$("button.Rarity").click(function() {
    if (!$(this).hasClass("Active")) {
        $(this).addClass("Active");
        Rarities.prepend($(this).parent());
        $('#Rarities #All').parent().remove();
        $('#rareModal').modal('toggle');
        rare.push($(this).attr("id"));
        if (rare.indexOf("All") > -1) {
            rare.splice(rare.indexOf("All"), 1);
        }
    } else {
        $(this).removeClass("Active");
        rareModal.prepend($(this).parent());
        rare.splice(rare.indexOf($(this).attr("id")), 1);
        if (Rarities.children().length <= 1) {
            Rarities.prepend(all);
            rare.push("All");
        }
    }
    //console.log(rare);
});

$("button#Search").click(function() {
    search();
});

function search() {
    //console.log(hero, qual, rare)
    var d = new Object();
    var lprice = $("#lprice").html(); //.replace("$","");
    var hprice = $("#hprice").html(); //.replace("$","");
    d.rare = rare
    d.qual = qual
    d.hero = hero
    url = "http://" + location.host + "/dota/search?h=" + encodeURIComponent(hero) + "&r=" + encodeURIComponent(rare) + "&q=" + encodeURIComponent(qual) + "&lp=" + encodeURIComponent(lprice) + "&hp=" + encodeURIComponent(hprice);
    location.href = url
};
