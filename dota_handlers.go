package main

import (
	"encoding/json"
	"fmt"
	"github.com/hoisie/mustache"
	"github.com/hoisie/web"
	"sort"
	"strings"
)

func dotaGet(ctx *web.Context) string {
	data := make(map[string]interface{})
	return mustache.RenderFileInLayout("templates/dota.html", "templates/base.html", data)
}

func heroesGet(ctx *web.Context) string {
	data := make(map[string]interface{})
	heroes := []*Hero{}
	for _, h := range HeroCache {
		if len(h.MarketItems()) > 0 {
			heroes = append(heroes, h)
		}
	}
	sort.Sort(ByName(heroes))
	data["heroes"] = heroes
	data["cur"] = getUserCurrency(ctx)
	return mustache.RenderFileInLayout("templates/heroes.html", "templates/base.html", data)
}

func searchGet(ctx *web.Context) string {
	data := make(map[string]interface{})

	params := ctx.Params
	//fmt.Println("Params:", params)
	var h = params["h"] // TODO only valid heroes
	var r = params["r"] // TODO only valid quals
	var q = params["q"] // TODO only valid rares

	var cur = getUserCurrency(ctx)
	data["cur"] = cur

	// filter out submitted heroes that aren't actual heroes, no h=bullshit
	if h != "All" {
		var rh = []string{}
		for _, z := range strings.Split(h, ",") {
			for _, y := range HeroCache {
				if strings.ToLower(z) == strings.ToLower(y.Name) {
					rh = append(rh, z)
				}
			}
		}
		h = strings.Join(rh, ",")
	}

	// filter out submitted rarities that aren't actual rarities, no r=bullshit
	if r != "All" {
		var rr = []string{}
		for _, z := range strings.Split(r, ",") {
			for _, y := range RARITIES {
				if strings.ToLower(z) == strings.ToLower(y) {
					rr = append(rr, z)
				}
			}
		}
		r = strings.Join(rr, ",")
	}

	// filter out submitted qualities that aren't actual qualities, no q=bullshit
	if q != "All" {
		var rq = []string{}
		for _, z := range strings.Split(q, ",") {
			for _, y := range QUALITIES {
				if strings.ToLower(z) == strings.ToLower(y) {
					rq = append(rq, z)
				}
			}
		}
		q = strings.Join(rq, ",")
	}

	lp := params["lp"]
	hp := params["hp"]

	// convert these to USD if alt currency
	// if euro, lp = lp / euro rate
	// hp = hp / euro rate
	if cur != nil {
		lp = ReversePrice(lp, cur.Code)
		hp = ReversePrice(hp, cur.Code)
	}

	if lp == "" || !ValidPrice(lp) || lp == " 0.00" || lp == "0.00" {
		lp = MINPRICE
	}
	if hp == "" || !ValidPrice(hp) || hp == " 0.00" || hp == "0.00" {
		hp = MAXPRICE
	}

	// if all variables are valid, fetch the items
	if h != "" && r != "" && q != "" && lp != "" && hp != "" {
		items := FilterBucket(h, r, q, lp, hp)
		// sort
		sort.Sort(ByPrice(items))
		data["items"] = items
		if len(items) > 0 {
			data["derp"] = true
		}
	}

	// build a list of hero objects from the get param
	hl := []*Hero{}
	for _, z := range strings.Split(h, ",") {
		t := getHero(z)
		if t != nil {
			hl = append(hl, t)
		}
	}

	// filter active/passive heroes, creating a list of heroes the user
	// hasn't searched for
	heroes := []*Hero{}
	for _, he := range HeroCache {
		found := false
		for _, hh := range hl {
			if hh.Name == he.Name {
				found = true
			}
		}
		if !found {
			if len(he.Items()) > 0 {
				heroes = append(heroes, he)
			}
		}
	}
	sort.Sort(ByName(heroes))
	data["heroes"] = heroes

	sort.Sort(ByName(hl))
	data["h"] = hl

	// filter active/passive qualities
	aq := strings.Split(q, ",")
	pq := []string{}
	for _, qu := range QUALITIES {
		found := false
		for _, qa := range aq {
			if strings.ToLower(qa) == strings.ToLower(qu) {
				found = true
			}
		}
		if !found {
			pq = append(pq, qu)
		}
	}
	if len(aq) > 0 && aq[0] != "" {
		data["q"] = aq
	}
	data["qualities"] = pq

	// filter active/passive rarities
	ar := strings.Split(r, ",")
	pr := []string{}
	for _, ru := range RARITIES {
		found := false
		for _, ra := range ar {
			if strings.ToLower(ra) == strings.ToLower(ru) {
				found = true
			}
		}
		if !found {
			pr = append(pr, ru)
		}
	}

	if len(ar) > 0 && ar[0] != "" {
		data["r"] = ar
	}

	data["rarities"] = pr

	data["minprice"] = MINPRICE
	data["maxprice"] = MAXPRICE
	if cur != nil {
		data["minprice"] = ConvertPrice(MINPRICE, cur.Code)
		data["maxprice"] = ConvertPrice(MAXPRICE, cur.Code)
		data["cur"] = cur
	}
	data["lp"] = lp
	data["hp"] = hp
	return mustache.RenderFileInLayout("templates/items.html", "templates/base.html", data)
}

func itemsPost(ctx *web.Context) string {
	params := ctx.Params
	var heroes []string
	var rarities []string
	var qualities []string
	_ = json.Unmarshal([]byte(params["hero"]), &heroes)    // TODO only valid heroes
	_ = json.Unmarshal([]byte(params["qual"]), &qualities) // TODO only valid quals
	_ = json.Unmarshal([]byte(params["rare"]), &rarities)  // TODO only valid rares
	lowprice := params["lprice"]
	highprice := params["hprice"]
	if lowprice == "" || !ValidPrice(lowprice) {
		lowprice = "0.01"
	}
	if highprice == "" || !ValidPrice(highprice) {
		highprice = "400.00"
	}
	bucket := FilterBucket(strings.Join(heroes, ","), strings.Join(rarities, ","), strings.Join(qualities, ","), lowprice, highprice)
	j, e := json.Marshal(bucket)
	if e == nil {
		return string(j)
	} else {
		fmt.Println("Error:", e)
		return "Errors abound!"
	}
}

func miscsGet(ctx *web.Context) string {
	data := make(map[string]interface{})
	var slots []*MiscBucket
	for _, k := range MiscCache {
		if len(k.Items()) > 0 {
			slots = append(slots, k)
		}
	}
	sort.Sort(MiscsName(slots))
	data["slots"] = slots
	data["cur"] = getUserCurrency(ctx)
	return mustache.RenderFileInLayout("templates/miscs.html", "templates/base.html", data)
}

func miscGet(ctx *web.Context, slot string) string {
	data := make(map[string]interface{})

	var cur = getUserCurrency(ctx)
	data["cur"] = cur

	misc := MiscCache[strings.ToLower(slot)]
	if misc == nil {
		ctx.Redirect(302, "/player")
		return ""
	}
	data["slot"] = slot
	data["misc"] = misc
	return mustache.RenderFileInLayout("templates/misc.html", "templates/base.html", data)
}

func heroGet(ctx *web.Context, hero string) string {
	data := make(map[string]interface{})
	var he *Hero
	for _, h := range HeroCache {
		if strings.ToLower(h.Name) == strings.ToLower(hero) {
			he = h
		}
	}
	if he == nil {
		ctx.Redirect(302, "/heroes")
		return ""
	}
	data["hero"] = he
	data["cur"] = getUserCurrency(ctx)
	return mustache.RenderFileInLayout("templates/hero.html", "templates/base.html", data)
}

func heroSlotGet(ctx *web.Context, hero string, slot string) string {
	var h *Hero
	for _, he := range HeroCache {
		if strings.ToLower(he.Name) == strings.ToLower(hero) {
			h = he
			break
		}
	}
	if h == nil {
		ctx.Redirect(302, "/dota/heroes")
		return ""
	}
	s := h.Slot(slot)
	if len(s.items) < 1 {
		ctx.Redirect(302, "/dota/hero/"+hero)
		return ""
	}

	data := make(map[string]interface{})
	var cur = getUserCurrency(ctx)
	data["hero"] = h
	data["slot"] = s
	data["cur"] = cur
	return mustache.RenderFileInLayout("templates/slot.html", "templates/base.html", data)
}

func heroItemGet(ctx *web.Context, hero string, slot string, item string) string {
	data := make(map[string]interface{})
	var cur = getUserCurrency(ctx)
	data["cur"] = cur
	it := BaseItemCache[strings.ToLower(item)]
	if it == nil {
		ctx.Redirect(302, "/dota/hero/"+hero+"/"+slot)
		return ""
	}
	data["item"] = it
	if strings.ContainsAny(strings.ToLower(string(it.Rarity()[0])), "aeiou") {
		data["addn"] = "n"
	}
	return mustache.RenderFileInLayout("templates/item.html", "templates/base.html", data)
}

func itemGet(ctx *web.Context, item string) string {
	data := make(map[string]interface{})

	var cur = getUserCurrency(ctx)
	data["cur"] = cur

	it := BaseItemCache[strings.ToLower(item)]
	if it == nil {
		ctx.Redirect(302, "/dota")
		return ""
	}
	data["item"] = it
	if strings.ContainsAny(strings.ToLower(string(it.Rarity()[0])), "aeiou") {
		data["addn"] = "n"
	}
	if it.slotname == "treasure chest" {
		data["box"] = true
	}
	return mustache.RenderFileInLayout("templates/leitem.html", "templates/base.html", data)
}

func setGet(ctx *web.Context, setstring string) string {
	data := make(map[string]interface{})

	var cur = getUserCurrency(ctx)
	data["cur"] = cur

	var s *Set
	for _, set := range SetCache {
		if strings.ToLower(set.Name) == strings.ToLower(setstring) {
			s = set
			break
		}
	}
	if s == nil {
		ctx.Redirect(302, "/dota/heroes")
		return ""
	}
	data["set"] = s
	return mustache.RenderFileInLayout("templates/set.html", "templates/base.html", data)
}

func priceStreamGet(ctx *web.Context) string {
	data := make(map[string]interface{})
	return mustache.RenderFileInLayout("templates/pricestream.html", "templates/base.html", data)
}

func DotA404Handler(ctx *web.Context, lost string) {
	ctx.Redirect(302, "/dota")
}
