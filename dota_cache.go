package main

import (
	"database/sql"
	"fmt"
	"strings"
)

//var imgWidth = "405f"  //"450f"
//var imgHeight = "270f" //"300f"

// cache vars
var ItemCache = make(map[string]map[string]*Item)
var BaseItemCache = make(map[string]*BaseItem)
var SetCache = make(map[string]*Set)
var HeroCache = make(map[string]*Hero)
var SlotCache = make(map[string]map[string]*Slot)
var MiscCache = make(map[string]*MiscBucket)
var LootCache = make(map[string]*LootList)

func CacheDota() {
	CacheHeroes()
	CacheSlots()
	CacheSets()
	CacheItems()
	CacheBaseItems()
	CachePlayerItems()
	CacheLootLists()
}

func CacheLootLists() {
	var LC = make(map[string]*LootList)
	ir, _ := db.Query("select distinct name from dotalootlist;")
	var name sql.NullString
	fmt.Println("Caching Loot Lists.")
	count := 0
	for ir.Next() {
		ll := &LootList{}
		ir.Scan(&name)
		ll.name = name.String
		items, _ := db.Query("select unnest(contents) as item from dotalootlist where name like $1;", name.String)
		var its []string
		for items.Next() {
			var item sql.NullString
			items.Scan(&item)
			its = append(its, item.String)
		}
		ll.items = its
		LC[strings.ToLower(name.String)] = ll
		count = count + 1
	}
	LootCache = LC
	fmt.Println(count, "lists cached.")
}

func CacheSlots() {
	var SC = make(map[string]map[string]*Slot)
	fmt.Println("Caching slots.")
	count := 0
	ir, _ := db.Query("select distinct name, heroid from dotahero order by heroid;")
	var name sql.NullString
	var heroid sql.NullString
	var heroids []string
	for ir.Next() {
		ir.Scan(&name, &heroid)
		heroids = append(heroids, heroid.String)
		SC[strings.ToLower(name.String)] = make(map[string]*Slot)
	}
	slots, _ := db.Query("select distinct slot, heroid from dotabaseitem where heroid is not null;")
	for slots.Next() {
		count = count + 1
		var slot sql.NullString
		var heroid sql.NullString
		slots.Scan(&slot, &heroid)
		for _, n := range heroids {
			//fmt.Println("n", strings.ToLower(n), "heroid", strings.ToLower(heroid.String))
			if strings.ToLower(n) == strings.ToLower(heroid.String) {
				s := CacheSlot(slot.String, heroid.String)
				SC[strings.ToLower(s.hero)][strings.ToLower(s.name)] = s
			}
		}
	}
	SlotCache = SC
	fmt.Println(count, "slots cached.")
}

func CacheSlot(slot string, hero string) *Slot {
	s := &Slot{}
	s.name = slot
	h, _ := db.Query("select distinct name from dotahero where lower(heroid) = lower($1);", hero)
	for h.Next() {
		var name sql.NullString
		h.Scan(&name)
		if len(name.String) > 0 {
			s.hero = name.String
		}
	}
	it, _ := db.Query("select distinct name from dotabaseitem where lower(slot) = lower($1) and lower(heroid) = lower($2);", s.name, hero)
	items := []string{}
	for it.Next() {
		var name sql.NullString
		it.Scan(&name)
		if len(name.String) > 0 {
			items = append(items, name.String)
		}
	}
	s.items = items
	return s
}

func CacheSets() {
	var SC = make(map[string]*Set)
	count := 0
	fmt.Println("Caching sets.")
	sets, _ := db.Query("select id, name, image, storebundle from dotaset;")

	for sets.Next() {
		count = count + 1
		var id sql.NullString
		var name sql.NullString
		var image sql.NullString
		var storebundle sql.NullString
		sets.Scan(&id, &name, &image, &storebundle)
		items, _ := db.Query("select unnest(items) as item from dotaset where name like $1;", name.String)
		var its []string
		for items.Next() {
			var item sql.NullString
			items.Scan(&item)
			its = append(its, item.String)
		}
		set := &Set{}
		set.Id = id.String
		set.Name = name.String
		set.Image = image.String
		set.StoreBundle = storebundle.String
		set.items = its
		SC[id.String] = set
	}
	SetCache = SC
	fmt.Println(count, "sets cached.")
}

func CacheHeroes() {
	var HC = make(map[string]*Hero)
	ir, _ := db.Query("select distinct name, image, heroid from dotahero order by name;")
	var name sql.NullString
	var image sql.NullString
	var heroid sql.NullString
	fmt.Println("Caching heroes.")
	count := 0
	for ir.Next() {
		hero := &Hero{}
		ir.Scan(&name, &image, &heroid)
		HEROES = append(HEROES, name.String)
		hero.Name = name.String
		hero.Image = image.String
		hero.Heroid = heroid.String
		HC[strings.ToLower(heroid.String)] = hero
		count = count + 1
	}
	HeroCache = HC
	fmt.Println(count, "heroes cached.")
}

func CacheItems() {
	var IC = make(map[string]map[string]*Item)
	fmt.Println("Caching items.")
	ir, _ := db.Query("select name, link, price, rarity, base, quality, count, image from dotaitem;")
	var name sql.NullString
	var link sql.NullString
	var price sql.NullString
	var rarity sql.NullString
	var base sql.NullString
	var quality sql.NullString
	var count sql.NullString
	var image sql.NullString
	var ct int

	for ir.Next() {
		ir.Scan(&name, &link, &price, &rarity, &base, &quality, &count, &image)
		i := &Item{}
		i.Name = name.String
		i.price = "0.00"
		if price.String != "" {
			i.price = price.String
		}
		i.Link = link.String
		i.Rarity = rarity.String
		i.base = base.String
		i.Quality = quality.String
		i.Count = count.String
		i.image = image.String
		if _, ok := IC[strings.ToLower(i.base)]; !ok {
			IC[strings.ToLower(i.base)] = make(map[string]*Item)
		}
		IC[strings.ToLower(i.base)][strings.ToLower(i.Name)] = i
		ct = ct + 1
	}
	ItemCache = IC
	fmt.Println("Cached", ct, "items.")
}

func CacheBaseItems() {
	var BIC = make(map[string]*BaseItem)
	fmt.Println("Caching base items.")
	ir, _ := db.Query("select name, description, image, slot, set, type, creationdate, heroid, model, particle, lootlist, storeprice, rarity from dotabaseitem;")
	var name sql.NullString
	var description sql.NullString
	var image sql.NullString
	var slot sql.NullString
	var set sql.NullString
	var Type sql.NullString
	var creationdate sql.NullString
	var heroid sql.NullString
	var model sql.NullString
	var particle sql.NullString
	var lootlist sql.NullString
	var storeprice sql.NullString
	var rarity sql.NullString
	for ir.Next() {
		ir.Scan(&name, &description, &image, &slot, &set, &Type, &creationdate, &heroid, &model, &particle, &lootlist, &storeprice, &rarity)
		i := &BaseItem{}
		i.Name = name.String
		i.Description = description.String
		i.image = image.String
		i.slotname = slot.String
		i.set = set.String
		i.Type = Type.String
		i.creationdate = creationdate.String
		i.Heroid = heroid.String
		i.Model = model.String
		i.Particle = particle.String
		i.lootlist = lootlist.String
		i.StorePrice = storeprice.String
		i.rarity = rarity.String
		if !strings.Contains(i.slotname, "retired") {
			BIC[strings.ToLower(i.Name)] = i
		}
	}
	BaseItemCache = BIC
	fmt.Println("Cached", len(BaseItemCache), "base items.")
}

func CachePlayerItems() {
	var MC = make(map[string]*MiscBucket)
	fmt.Println("Caching misc items")
	count := 0
	slotq, _ := db.Query("select distinct slot from dotabaseitem where heroid is null;")
	slots := []string{}
	for slotq.Next() {
		var slot sql.NullString
		slotq.Scan(&slot)
		s := slot.String
		if s != "none" && s != "retired treasure chest" {
			slots = append(slots, slot.String)
		}
	}
	for _, slot := range slots {
		MC[slot] = &MiscBucket{name: slot}
	}
	for _, item := range BaseItemCache {
		for _, slot := range slots {
			if item.slotname == slot && item.OnMarket() == true && len(item.Heroid) == 0 {
				sloots := MC[slot]
				sloots.items = append(sloots.items, item)
				MC[slot] = sloots
				count = count + 1
			}
		}
	}
	MiscCache = MC
	fmt.Println("Cached", count, "misc items in", len(MiscCache), "categories.")
}
